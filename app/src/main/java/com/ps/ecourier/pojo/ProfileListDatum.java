package com.ps.ecourier.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */
public class ProfileListDatum {

    private String space = " ";
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("join_date")
    @Expose
    private String joinDate;
    @SerializedName("delivery_zone")
    @Expose
    private String deliveryZone;
    @SerializedName("avg_review")
    @Expose
    private String avgReview;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getDeliveryZone() {
        return deliveryZone;
    }

    public void setDeliveryZone(String deliveryZone) {
        this.deliveryZone = deliveryZone;
    }


    public String getAvgReview() {
        return avgReview;
    }

    public void setAvgReview(String avgReview) {
        this.avgReview = avgReview;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    @Override
    public String toString() {
        return name
                +space+joinDate
                +space+deliveryZone
                +space+avgReview
                +space+profilePic;
    }
}
