package com.ps.ecourier.pojo;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReportsListDatum implements Serializable {

    @SerializedName("consignment_id")
    @Expose
    private String consignmentId;
    @SerializedName("sender_group")
    @Expose
    private String sender_group;
    @SerializedName("eso")
    @Expose
    private String eso;
    @SerializedName("eso_contact")
    @Expose
    private String eso_contact;
    @SerializedName("recipient_name")
    @Expose
    private String recipientName;
    @SerializedName("recipient_mobile")
    @Expose
    private String recipientMobile;
    @SerializedName("recipient_address")
    @Expose
    private String recipient_address;
    @SerializedName("recipient_area")
    @Expose
    private String recipient_area;
    @SerializedName("order_time")
    @Expose
    private String orderTime;
    @SerializedName("requested_delivery_time")
    @Expose
    private String requested_delivery_time;
    @SerializedName("actual_delivery_time")
    @Expose
    private String actual_delivery_time;
    @SerializedName("pick_up_agent")
    @Expose
    private String pick_up_agent;
    @SerializedName("delivery_agent")
    @Expose
    private String delivery_agent;
    @SerializedName("parcel_status")
    @Expose
    private String parcelStatus;
    @SerializedName("status_code")
    @Expose
    private String status_code;
    @SerializedName("shipping_price")
    @Expose
    private String shippingPrice;
    @SerializedName("product_price")
    @Expose
    private String productPrice;
    @SerializedName("cod_price")
    @Expose
    private String codPrice;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("parcel_id")
    @Expose
    private String parcelId;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("payment_clear")
    @Expose
    private String paymentClear;
    @SerializedName("reference_id")
    @Expose
    private String reference_id;
    @SerializedName("items")
    @Expose
    private String items;

    public String getRecipient_address() {
        return recipient_address;
    }

    public void setRecipient_address(String recipient_address) {
        this.recipient_address = recipient_address;
    }

    public String getRecipient_area() {
        return recipient_area;
    }

    public void setRecipient_area(String recipient_area) {
        this.recipient_area = recipient_area;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getCollected_amount() {
        return collected_amount;
    }

    public void setCollected_amount(String collected_amount) {
        this.collected_amount = collected_amount;
    }

    @SerializedName("collected_amount")
    @Expose
    private String collected_amount;

    public String getCodPrice() {
        return codPrice;
    }

    public void setCodPrice(String codPrice) {
        this.codPrice = codPrice;
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getSender_group() {
        return sender_group;
    }

    public void setSender_group(String sender_group) {
        this.sender_group = sender_group;
    }

    public String getEso() {
        return eso;
    }

    public void setEso(String eso) {
        this.eso = eso;
    }

    public String getEso_contact() {
        return eso_contact;
    }

    public void setEso_contact(String eso_contact) {
        this.eso_contact = eso_contact;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientMobile() {
        return recipientMobile;
    }

    public void setRecipientMobile(String recipientMobile) {
        this.recipientMobile = recipientMobile;
    }

    public String getRecipientAddress() {
        return recipient_address;
    }

    public void setRecipientAddress(String recipient_address) {
        this.recipient_address = recipient_address;
    }

    public String getRecipientArea() {
        return recipient_area;
    }

    public void setRecipientArea(String recipient_area) {
        this.recipient_area = recipient_area;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getRequested_delivery_time() {
        return requested_delivery_time;
    }

    public void setRequested_delivery_time(String requested_delivery_time) {
        this.requested_delivery_time = requested_delivery_time;
    }

    public String getActual_delivery_time() {
        return actual_delivery_time;
    }

    public void setActual_delivery_time(String actual_delivery_time) {
        this.actual_delivery_time = actual_delivery_time;
    }

    public String getPick_up_agent() {
        return pick_up_agent;
    }

    public void setPick_up_agent(String pick_up_agent) {
        this.pick_up_agent = pick_up_agent;
    }

    public String getDelivery_agent() {
        return delivery_agent;
    }

    public void setDelivery_agent(String delivery_agent) {
        this.delivery_agent = delivery_agent;
    }

    public String getParcelStatus() {
        return parcelStatus;
    }

    public void setParcelStatus(String parcelStatus) {
        this.parcelStatus = parcelStatus;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getShippingPrice() {
        return shippingPrice;
    }

    public void setShippingPrice(String shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getParcelId() {
        return parcelId;
    }

    public void setParcelId(String parcelId) {
        this.parcelId = parcelId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPaymentClear() {
        return paymentClear;
    }

    public void setPaymentClear(String paymentClear) {
        this.paymentClear = paymentClear;
    }

    public String getReference_id() {
        return reference_id;
    }

    public void setReference_id(String reference_id) {
        this.reference_id = reference_id;
    }
}