package com.ps.ecourier.webservices.interfaces;

import com.ps.ecourier.pojo.Status;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Copyright (c) kazi srabon 2017. Contact, kaziiit@gmail.com
 */
public interface AgentEditProfile {
    @FormUrlEncoded
    @POST("/api/{operation}")
    public void getResult(
            @Path("operation") String operation,
            @FieldMap Map<String, String> params,
            Callback<Status> response
    );
}
