/*
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */

package com.ps.ecourier.webservices.interfaces;

import com.ps.ecourier.pojo.Login;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.Path;

public interface LoginInterface {
    @FormUrlEncoded
    @POST("/api/{operation}")
    public void getResult(
            @Path("operation") String operation,
            @FieldMap Map<String, String> params,
            Callback<Login> response
    );
}


