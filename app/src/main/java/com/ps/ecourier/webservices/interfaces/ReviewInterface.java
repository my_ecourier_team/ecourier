package com.ps.ecourier.webservices.interfaces;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com.
 */

import com.ps.ecourier.pojo.StatusAlternative;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.GET;
import retrofit.http.Path;

public interface ReviewInterface {
    //string operation is for passing values while calling the interface
//    @POST("/api/{operation}")
    @FormUrlEncoded
    @POST("/api/{operation}")
    /*
    operation is for specifying the api action
    FiledMap is used for passing the request parameters
    response is the response from the server which is now in the POJO
    */
    public void getResult(
            @Path("operation") String operation,
            @FieldMap Map<String, String> params,
            Callback<StatusAlternative> response
    );
}
