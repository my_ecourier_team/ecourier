package com.ps.ecourier.utils;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */

public class LogUtils {
    static final boolean log = false;

    public static void i(String tag, String string) {
        if (log) android.util.Log.i(tag, string);
    }

    public static void e(String tag, String string) {
        if (log) android.util.Log.e(tag, string);
    }

    public static void d(String tag, String string) {
        if (log) android.util.Log.d(tag, string);
    }

    public static void v(String tag, String string) {
        if (log) android.util.Log.v(tag, string);
    }

    public static void w(String tag, String string) {
        if (log) android.util.Log.w(tag, string);
    }
}