package com.ps.ecourier.activities.firstLayer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Toast;

import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.mikepenz.crossfadedrawerlayout.view.CrossfadeDrawerLayout;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.MiniDrawer;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.interfaces.ICrossfader;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;
import com.mikepenz.materialdrawer.util.KeyboardUtil;
import com.mikepenz.materialize.util.UIUtils;
import com.ps.ecourier.R;
import com.ps.ecourier.activities.LoginActivity;
import com.ps.ecourier.activities.MainActivity;
import com.ps.ecourier.base.BaseActivity;
import com.ps.ecourier.pojo.AnnouncementList;
import com.ps.ecourier.session.SessionUserData;
import com.ps.ecourier.webservices.ApiParams;
import com.ps.ecourier.webservices.interfaces.AnnouncementListInterface;

import org.jsoup.Jsoup;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.ps.ecourier.activities.MainActivity.user;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */
public class ActivityCheckVersion extends BaseActivity {

    private static final String url = "https://play.google.com/store/apps/details?id=com.ps.ecourier";
    private WebView web;

//    drawer setup
    private AccountHeader headerResult = null;
    private Drawer result = null;
    private CrossfadeDrawerLayout crossfadeDrawerLayout = null;
    private IProfile profile;
    private MiniDrawer miniResult = null;
    private Context mContext;
    private Activity mActivity;
    private SessionUserData sessionUserData;
    private boolean hasNew = false;
    private String TAG = "Activity checkV";
    private String nAnnouncement;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_check_version);
        initDrawer(savedInstanceState);
        web = (WebView) findViewById(R.id.webView);
        goToPlay();
    }

    private void goToPlay(){
        web.loadUrl(url);
    }

    private void initDrawer(Bundle savedInstanceState) {
        mContext = ActivityCheckVersion.this;
        mActivity = ActivityCheckVersion.this;
        Log.e(TAG, user_type+ " "+ user_type_name);
        setProfile(MainActivity.user_name, MainActivity.user_type_name, MainActivity.user_pro_pic);
        getAnnouncementNumber(savedInstanceState);
    }

    private void getAnnouncementNumber(final Bundle savedInstanceState) {
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(ApiParams.TAG_BASE_URL).build();
        //creating a service for adapter with our ApiCallback class
        AnnouncementListInterface myApiCallback = restAdapter.create(AnnouncementListInterface.class);

        // get user data from session

        String user_type = user.get(SessionUserData.KEY_USER_TYPE);
        String id = user.get(SessionUserData.KEY_USER_ID);
        String group = user.get(SessionUserData.KEY_USER_GROUP);
        String authentication_key = user.get(SessionUserData.KEY_USER_AUTH_KEY);
        Log.e(TAG, user_type+" "+id+" "+group+" "+authentication_key);

        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(ApiParams.PARAM_AGENT_ID, id);
        map.put(ApiParams.PARAM_GROUP, group);
        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, authentication_key);
        map.put(ApiParams.PARAM_ANNOUNCEMENT_ID, "1");
        myApiCallback.getData(ApiParams.TAG_ANNOUNCEMENT_KEY, map, new Callback<AnnouncementList>() {
            @Override
            public void success(AnnouncementList announcementList, Response response) {
                nAnnouncement = announcementList.getNo_of_items();
                setDrawer(savedInstanceState, mActivity, true, getString(R.string.nav_item_check_update), MainActivity.activity_update, nAnnouncement);
            }

            @Override
            public void failure(RetrofitError error) {
                nAnnouncement = "0";
                setDrawer(savedInstanceState, mActivity, true, getString(R.string.nav_item_check_update), MainActivity.activity_update, nAnnouncement);
            }
        });
    }


    public void setDrawer(Bundle savedInstanceState, Activity activity, boolean isHome, String title,
                          int position, String no_announcement) {

        mContext = activity.getApplicationContext();
        mActivity = activity;
        sessionUserData = new SessionUserData(mContext);
        // Handle Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(isHome);
        getSupportActionBar().setTitle(title);
        buildHeader(savedInstanceState, mActivity);

        result = new DrawerBuilder()
                .withActivity(mActivity)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withDrawerLayout(R.layout.crossfade_drawer)
                .withDrawerWidthDp(72)
                .withGenerateMiniDrawer(true)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.title_home)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_home)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(0),
                        new PrimaryDrawerItem().withName(R.string.title_consignment_list)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_list_alt)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.title_reports)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_file_text)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(2),
                        new PrimaryDrawerItem().withName(R.string.title_profile)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_user)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.nav_item_announcement)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_bullhorn)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withBadge(no_announcement)
                                .withBadgeStyle(new BadgeStyle(Color.RED, Color.RED))
                                .withIdentifier(4),
                        new PrimaryDrawerItem().withName(R.string.nav_item_check_update)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_globe)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(5)
                                .withEnabled(false),
                        new PrimaryDrawerItem().withName(R.string.nav_item_attendence)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_check)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(6),
                        new PrimaryDrawerItem().withName(R.string.nav_item_logout)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_sign_out)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(7)
                ) // add the items we want to use with our Drawer
                .withOnDrawerNavigationListener(new Drawer.OnDrawerNavigationListener() {
                    @Override
                    public boolean onNavigationClickListener(View clickedView) {
                        //this method is only called if the Arrow icon is shown. The hamburger is automatically managed by the MaterialDrawer
                        //if the back arrow is shown. close the activity
//                        mActivity.finish();
                        //return true if we have consumed the event
                        return true;
                    }
                })
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
//                        Toast.makeText(MapsActivity.this, "onDrawerOpened", Toast.LENGTH_SHORT).show();
                        KeyboardUtil.hideKeyboard(mActivity);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
//                        Toast.makeText(MapsActivity.this, "onDrawerClosed", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem instanceof Nameable) {
                            Toast.makeText(mContext, ((Nameable) drawerItem).getName()
                                    .getText(mContext), Toast.LENGTH_SHORT).show();
                            if (drawerItem.getIdentifier() == 0) {
                                startActivity(new Intent(mContext, MainActivity.class));
                            }
                            else if (drawerItem.getIdentifier() == 1) {
                                startActivity(new Intent(mContext, ActivityConsignmentList.class));
                            }
                            else if (drawerItem.getIdentifier() == 2) {
                                startActivity(new Intent(mContext, ActivityReports.class));
                            }
                            else if (drawerItem.getIdentifier() == 3) {
                                startActivity(new Intent(mContext, ActivityProfile.class));
                            }
                            else if (drawerItem.getIdentifier() == 4) {
                                startActivity(new Intent(mContext, AnnouncementActivity.class));
                            }
                            else if (drawerItem.getIdentifier() == 5) {
                                ActivityCheckVersion.GetVersionCode getVersionCode = new ActivityCheckVersion.GetVersionCode();
                                showProgressDialog(true, " Version Checking", "Please wait...");
                                getVersionCode.execute();
                            }
                            else if (drawerItem.getIdentifier() == 6) {
                                startActivity(new Intent(mContext, AttendenceActivity.class));
                            }
                            else if (drawerItem.getIdentifier() == 7) {
                                sessionUserData.endSession();
                                LoginActivity.deleteCache(mContext);
                                sessionUserData.checkLogin();
                                finish();
                            }
                            else if (drawerItem.getIdentifier() == 10) {
                                showToast("Settings", Toast.LENGTH_SHORT, 0);
//                                startActivity(new Intent(context, SettingsActivity.class));
                            }
                        }
                        return false;
                    }
                })
                .addStickyDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_settings)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_cog)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(10)
                )
                .withSelectedItem(position)
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .build();

        crossfadeDrawerLayout = (CrossfadeDrawerLayout) result.getDrawerLayout();
        crossfadeDrawerLayout.setMaxWidthPx(DrawerUIUtils.getOptimalDrawerWidth(this));
        miniResult = result.getMiniDrawer();
        View view = miniResult.build(this);
        view.setBackgroundColor(
                UIUtils.getThemeColorFromAttrOrRes(
                    this,
                    com.mikepenz.materialdrawer.R.attr.material_drawer_background,
                    com.mikepenz.materialdrawer.R.color.material_drawer_background));
        crossfadeDrawerLayout.getSmallView().addView(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        miniResult.withCrossFader(new ICrossfader() {
            @Override
            public void crossfade() {
                boolean isFaded = isCrossfaded();
                crossfadeDrawerLayout.crossfade(400);

                //only close the drawer if we were already faded and want to close it now
                if (isFaded) {
                    result.getDrawerLayout().closeDrawer(GravityCompat.START);
                }
            }

            @Override
            public boolean isCrossfaded() {
                return crossfadeDrawerLayout.isCrossfaded();
            }
        });

    }

    private void buildHeader(Bundle savedInstanceState, Activity activity) {
        //        popup init
        final ImagePopup imagePopup;imagePopup = new ImagePopup(this);
        imagePopup.setWindowWidth(800);
        imagePopup.setWindowHeight(800);
        imagePopup.setHideCloseIcon(true);
        imagePopup.setImageOnClickClose(true);

        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(activity)
                .withHeaderBackground(R.drawable.bg_nav_main)
                .addProfiles(profile)
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        String uName = profile.getName().getText();
                        if (user_name.equals(uName))
                            startActivity(new Intent(mContext, ActivityProfile.class));

                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        String uName = profile.getName().getText();
                        if (user_name.equals(uName)){
                            if (IMG_DRAWABLE != null)
                                imagePopup.initiatePopup(IMG_DRAWABLE);
//                            Bitmap bitmap = showProfileImageChangeDialog();
//
//                            if (bitmap != null){
//                                Log.e(TAG, bitmap.toString());
//                                mProfile = new ProfileDrawerItem().withName(user_name).withEmail(user_type).withIcon(bitmap);
//                                headerResult.updateProfile(mProfile);
//                            }
                        }
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();
    }

    public void setProfile(String name, String type, String pro_pic_url) {
        Log.e(TAG, pro_pic_url);
        if(pro_pic_url != null && !pro_pic_url.isEmpty() && !pro_pic_url.equals(""))
            profile = new ProfileDrawerItem().withName(name).withEmail(type).withIcon(pro_pic_url);

        else profile = new ProfileDrawerItem().withName(name).withEmail(type)
                .withIcon(getResources().getDrawable(R.drawable.ic_default_propic));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //add the values which need to be saved from the drawer to the bundle
        outState = result.saveInstanceState(outState);
        //add the values which need to be saved from the accountHeader to the bundle
        outState = headerResult.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    public class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + MainActivity.packageName + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (!MainActivity.currentVersion.equals(onlineVersion)) {
                    //show dialog
                    hasNew = true;
                    Log.e("Has New ", " "+hasNew);
                    startActivity(new Intent(mContext, ActivityCheckVersion.class));
                }
            }
            Log.e("update", "Current version " + MainActivity.currentVersion + "playstore version " + onlineVersion+" hasNext : "+hasNew);
            hideProgressDialog();
        }
    }

}
