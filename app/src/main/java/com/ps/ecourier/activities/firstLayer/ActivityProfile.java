package com.ps.ecourier.activities.firstLayer;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.datetimepicker.date.DatePickerDialog;
import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.mikepenz.crossfadedrawerlayout.view.CrossfadeDrawerLayout;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.MiniDrawer;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.interfaces.ICrossfader;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;
import com.mikepenz.materialdrawer.util.KeyboardUtil;
import com.mikepenz.materialize.util.UIUtils;
import com.ps.ecourier.R;
import com.ps.ecourier.activities.LoginActivity;
import com.ps.ecourier.activities.MainActivity;
import com.ps.ecourier.base.BaseActivity;
import com.ps.ecourier.pojo.AnnouncementList;
import com.ps.ecourier.pojo.ProfileList;
import com.ps.ecourier.pojo.Status;
import com.ps.ecourier.session.SessionUserData;
import com.ps.ecourier.webservices.ApiParams;
import com.ps.ecourier.webservices.interfaces.AgentEditProfile;
import com.ps.ecourier.webservices.interfaces.AnnouncementListInterface;
import com.ps.ecourier.webservices.interfaces.ProfileListInterface;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.ps.ecourier.activities.MainActivity.user;
import static com.ps.ecourier.activities.secondLayer.ActivityConsignmentDetails.getFolderSize;

public class ActivityProfile extends BaseActivity implements DatePickerDialog.OnDateSetListener{

    private AgentEditProfile myAgentEditProfile;
    private static final String[] PERMISSIONS_CAMERA = new String[]{Manifest.permission.CAMERA};
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static String[] PERMISSIONS_READ_STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE};
    private String[] blood_group;
    private File mPhoto;
    private String path = "", mBloodGroup = "", mDOB = "", mNID = "";
    private String user_type = "", id = "", group = "", authentication_key = "";
    private static final int SELECT_PICTURE = 1, TAKE_PHOTO = 2,MY_PERMISSIONS_REQUEST_CAMERA = 3,
            REQUEST_EXTERNAL_STORAGE = 4, REQUEST_READ_EXTERNAL_STORAGE = 5;
    private int calender_type = 1;
    private TextView txtName, txtJoinDate, txtDeliveryZone, txtRating, txtBG, txtDOB, txtNID;
    private AutoCompleteTextView editTextBG;
    private EditText editTextDOB, editTextNID;
    private Calendar calendar;
    private AlertDialog b;
    private Resources res;
    final private String TAG ="Fragment profile";
    private CircleImageView imgProfilePic;
    private Context context;
    private ImageView buttonEdit;
    private Button  buttonClear, buttonCancle, buttonUpdate;
    private FloatingActionButton btnSelectImage;
    private ProgressDialog mProgressDialog;
    ArrayList<String> image_list=new ArrayList<String>();
    ArrayList<Drawable> image_drawable=new ArrayList<Drawable>();
    private boolean returnType;
    public HashMap<String, String> user = new HashMap<String, String>();
    //    drawer setup
    private AccountHeader headerResult = null;
    private Drawer result = null;
    private CrossfadeDrawerLayout crossfadeDrawerLayout = null;
    private IProfile profile;
    private MiniDrawer miniResult = null;
    private Context mContext;
    private Activity mActivity;
    private SessionUserData sessionUserData;
    private boolean hasNew = false;
    private OkHttpClient client;
    private boolean isSent = false;
    private Button btnTakePhoto, btnOpenGallery;
    private String nAnnouncement;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getResources();
        verifyStoragePermissions(this);
        verifyCameraPermission(this);
        verifyReadStoragePermissions(this);
        // Inflate the layout for this fragment
        setContentView(R.layout.activity_profile_new);

        context = ActivityProfile.this;
        sessionUserData = new SessionUserData(this);
        user = sessionUserData.getSessionDetails();
        calendar = Calendar.getInstance();
        initialize();
        initDrawer(savedInstanceState);

        loadProfile();
    }

    private void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File folder = new File(Environment.getExternalStorageDirectory() + "/"+ALBUM_NAME);

        if(!folder.exists())
        {
            folder.mkdir();
        }
        final Calendar c = Calendar.getInstance();
//        String new_Date= c.get(Calendar.DAY_OF_MONTH)+"-"+((c.get(Calendar.MONTH))+1)   +"-"+c.get(Calendar.YEAR) +" " + c.get(Calendar.HOUR) + "-" + c.get(Calendar.MINUTE)+ "-"+ c.get(Calendar.SECOND);
//        path=String.format(Environment.getExternalStorageDirectory() +"/"+ALBUM_NAME+"/%s.jpg",ALBUM_NAME+"("+new_Date+")");
        path = String.format(Environment.getExternalStorageDirectory() +"/"+ALBUM_NAME+"_%d.jpg", System.currentTimeMillis());
        mPhoto = new File(path);
//        imgProfilePic.setImageURI(Uri.fromFile(mPhoto));
        Log.e("URI", Uri.fromFile(mPhoto).toString());
        Uri uri = FileProvider.getUriForFile(
                this,
                context.getApplicationContext().getPackageName()+".provider",
                mPhoto);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, TAKE_PHOTO);
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        file.mkdir();
        if (!file.exists()) {
            Log.e(ALBUM_NAME, "External Directory not created");
            file = new File(context.getFilesDir(), albumName);
            file.mkdir();
            if (!(file.exists() && file.isDirectory())){
                Log.e(ALBUM_NAME, "Internal Directory not created");
            }
        }
        return file;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_edit_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_edit:
                promptEditProfile();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initialize() {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Picture Uploading...");
        mProgressDialog.setCancelable(false);
        txtName = (TextView) findViewById(R.id.txtName);
        txtJoinDate = (TextView) findViewById(R.id.txtJoinDate);
        txtDeliveryZone = (TextView) findViewById(R.id.txtDeliveryZone);
        txtRating = (TextView) findViewById(R.id.txtRating);
        txtBG = (TextView) findViewById(R.id.txtBloodGroup);
        txtDOB = (TextView) findViewById(R.id.txtDOB);
        txtNID = (TextView) findViewById(R.id.txtNID);
        imgProfilePic = (CircleImageView) findViewById(R.id.user_profile_photo);
        imgProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptEditPP();
            }
        });
        btnSelectImage = (FloatingActionButton) findViewById(R.id.btnSelectImage);
        btnSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptEditPP();
            }
        });
    }

    private void promptEditPP() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ActivityProfile.this);
        LayoutInflater inflater = ActivityProfile.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_profile_pic, null);
        dialogBuilder.setView(dialogView);
        b = dialogBuilder.create();
        b.setCancelable(true);
        b.show();

        btnTakePhoto = (Button) dialogView.findViewById(R.id.btnTakePhoto);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.cancel();
                takePhoto();
            }
        });
        btnOpenGallery = (Button) dialogView.findViewById(R.id.btnOpenGallery);
        btnOpenGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.cancel();
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Gallery"), SELECT_PICTURE);
            }
        });
    }

    private void promptEditProfile() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_profile_edit, null);
        dialogBuilder.setView(dialogView);
        b = dialogBuilder.create();
        b.setCancelable(true);
        b.show();

        editTextBG = (AutoCompleteTextView) dialogView.findViewById(R.id.autoTextBG);
        editTextDOB = (EditText) dialogView.findViewById(R.id.textDOB);
        editTextNID = (EditText) dialogView.findViewById(R.id.textNID);
        buttonClear = (Button) dialogView.findViewById(R.id.btnProfileClear);
        buttonUpdate = (Button) dialogView.findViewById(R.id.btnProfileUpdate);
        buttonCancle = (Button) dialogView.findViewById(R.id.btnProfileCancle);

        blood_group = getResources().getStringArray(R.array.blood_group);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, blood_group);
        editTextBG.setAdapter(adapter);

        editTextDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calender_type = 0;
                DatePickerDialog.newInstance(ActivityProfile.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show(getFragmentManager(), "datePicker");

            }
        });

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAll();
            }
        });

        buttonCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAll();
                b.cancel();
            }
        });

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBloodGroup = editTextBG.getText().toString();
                mDOB = editTextDOB.getText().toString();
                mNID = editTextNID.getText().toString();

                updateUI(mBloodGroup, mNID, mDOB);
                callForUpdate(loadUserDataFromSession(mBloodGroup, mNID, mDOB));
                b.cancel();
            }
        });
    }

    private void updateUserProfile(String bloodGroup, String nID, String dOB) {

        loadUserDataFromSession(bloodGroup, nID, dOB);
    }

    private void resetAll() {
        String empty= "";
        editTextBG.setText(empty);
        editTextDOB.setText(empty);
        editTextNID.setText(empty);
    }

    /* Choose an image from Gallery */
    void openImageChooser() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                // Get the url from data
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    // Get the path from the Uri
                    String path = getPathFromURI(selectedImageUri);
                    mPhoto = new File(path);
                    Log.e(TAG, "Image file name : " + mPhoto.getName());
                    // Set the image in ImageView
                    try {
                        mBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
                        imgProfilePic.setImageBitmap(mBitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (mBitmap == null)
                        imgProfilePic.setImageURI(selectedImageUri);
                    Log.e(TAG, "Image file name : " + mPhoto.getName());
                    sendFile(bitmapToFile(getResizedBitmap(mBitmap, 50)));
//                    sendFile(mPhoto);
                }
            }
            if(requestCode == TAKE_PHOTO)
            {
                image_list.add(path);
                new GetImages().execute();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    showErrorToast("Cannot open camera", Toast.LENGTH_SHORT);
                }
            }
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    showErrorToast("Cannot write images to external storage", Toast.LENGTH_SHORT);
                }
            }
            case REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    showErrorToast("Cannot read images to external storage", Toast.LENGTH_SHORT);
                }
            }
        }
    }

    private void loadProfile() {

        showProgressDialog(false, "", getResources().getString(R.string.loading));

        //Retrofit section start from here...
        //create an adapter for retrofit with base url
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(ApiParams.TAG_BASE_URL).build();
        //creating a service for adapter with our ApiCallback class
        ProfileListInterface myApiCallback = restAdapter.create(ProfileListInterface.class);

        // get user data from session
        String user_type = user.get(SessionUserData.KEY_USER_TYPE);
        String id = user.get(SessionUserData.KEY_USER_ID);
        String group = user.get(SessionUserData.KEY_USER_GROUP);
        String authentication_key = user.get(SessionUserData.KEY_USER_AUTH_KEY);
        String name = user.get(SessionUserData.KEY_USER_NAME);
        final String profilePic = user.get(SessionUserData.KEY_USER_PROFILE_PICTURE); //set picasso
        final String avgReview= user.get(SessionUserData.KEY_USER_AVG_REVIEW); //set avg review
        txtName.setText(name);
        txtRating.setText(avgReview);
        Log.e(TAG, name+" "+avgReview+" "+id+" "+group+" "+authentication_key+" from sessionUserData");

        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();

        if (user_type.contains("1")) {
            map.put(ApiParams.PARAM_ADMIN_ID, "" + id);
        } else if (user_type.contains("2")) {
            map.put(ApiParams.PARAM_AGENT_ID, "" + id);
        }
        map.put(ApiParams.PARAM_GROUP, "" + group);
        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, "" + authentication_key);
//        map.put(ApiParams.PARAM_AVG_REVIEW, "" + avgReview);

        //Now, we will to call for response
        //Retrofit using gson for JSON-POJO conversion
        myApiCallback.getData(ApiParams.TAG_PROFILE_KEY, map, new Callback<ProfileList>() {
            @Override
            public void success(ProfileList profileList, Response response) {
                //we get json object from server to our POJO or model class

                hideProgressDialog();

                boolean status = profileList.getStatus();
                if (status) {
                    updateUI(profileList);
                    String url = "";
                    url = profileList.getData().getProfilePic();
                    if (checkText(url)){
                        if(url.contains("test."))
                            url = url.replace("test.", "");
                        url = "http://"+url;
                        Log.e(TAG, url);
                        Picasso.with(context)
                                .load(url)
                                .skipMemoryCache()
                                .error(R.drawable.ic_profile_pic)
                                .into(imgProfilePic);
                    }
                    if(!(avgReview.equals("") || avgReview.equals("0"))){
                        txtRating.setText(avgReview);
                    }else if(!profileList.getData().getAvgReview().equals("")){
                        txtRating.setText(profileList.getData().getAvgReview());
                    }else {
                        txtRating.setText("NG");
                    }
                } else {
                    showErrorToast(getString(R.string.no_data_found), 0);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialog();
                showErrorToast("" + error.getMessage() + "!", 0);
            }
        });
    }

    private void updateUI(ProfileList profileList) {
        if(checkText(profileList.getData().getName()))
            txtName.setText("Name: " + profileList.getData().getName());
        if(checkText(profileList.getData().getJoinDate()))
            txtJoinDate.setText("Joining Date: " + profileList.getData().getJoinDate());
        if (checkText(profileList.getData().getDeliveryZone()))
            txtDeliveryZone.setText("Delivery Zone: " + profileList.getData().getDeliveryZone());
        if(checkText(user.get(SessionUserData.KEY_USER_BLOOD_GROUP)))
            txtBG.setText("Blood Group: "+ user.get(SessionUserData.KEY_USER_BLOOD_GROUP));
        if(checkText(user.get(SessionUserData.KEY_USER_DOB)))
            txtDOB.setText("Date of Birth: "+ user.get(SessionUserData.KEY_USER_DOB));
        if (checkText(user.get(SessionUserData.KEY_USER_NID)))
            txtNID.setText("*****************");
    }

    private void updateUI(String bloodGroup, String nID, String dOB){
        if(checkText(bloodGroup))
            txtBG.setText("Blood Group: "+ bloodGroup);
        if(checkText(dOB))
            txtDOB.setText("Date of Birth: "+ dOB);
        if (checkText(nID))
            txtNID.setText("*****************");
    }

    private boolean checkText(String s) {
        boolean isValid;
        if(s != null && !s.isEmpty())
            isValid = true;
        else isValid = false;

        return isValid;
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(year, monthOfYear, dayOfMonth);
        updateEditTextofDOB();
    }

    private void updateEditTextofDOB() {
        SimpleDateFormat sdf_date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        //SimpleDateFormat sdf_time = new SimpleDateFormat("HH:mm:ss");

        if (calender_type == 0) {
            editTextDOB.setText(sdf_date.format(calendar.getTime()));
        } else
            editTextDOB.setText("");
        Log.e(TAG, "DOB "+sdf_date.format(calendar.getTime()));
    }

    private void sendFile(File file){
        long size = getFolderSize(file)/1024;
        Log.e("File Size ", ""+size);
        String user_type = user.get(SessionUserData.KEY_USER_TYPE);
        String id = user.get(SessionUserData.KEY_USER_ID);
        String group = user.get(SessionUserData.KEY_USER_GROUP);
        String authentication_key = user.get(SessionUserData.KEY_USER_AUTH_KEY);

        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();

        client = new OkHttpClient();
//        MediaType mediaType = MediaType.parse()
        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addFormDataPart("authentication_key", authentication_key)
                .addFormDataPart("agent_id", id)
                .addFormDataPart("group", group)
                .addFormDataPart("filename", file.getName(),
                        RequestBody.create(MediaType.parse("jpg"), file))
                .build();
        Log.e(TAG, id+" "+group+" "+authentication_key+" "+file.getName());
        String serverURL = "";
        serverURL = ApiParams.TAG_BASE_URL + "/api/"+ApiParams.TAG_PROFILE_UPDATE_KEY;
        Log.e("Server Url", serverURL);
        Request request = new Request.Builder()
                .url(serverURL)
                .post(requestBody)
                .build();
        showProgressDialog(false, "", "Sending...");
        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "Failed");
                hideProgressDialog();
            }
            @Override
            public void onResponse(com.squareup.okhttp.Response response) throws IOException {
                Log.e(TAG, response.message());
                hideProgressDialog();
            }
        });
    }

//    private class SendFile extends AsyncTask <File, Void, Void>
//    {
//        @Override
//        protected void onPreExecute() {
//            showProgressDialog(false, "", "Sending...");
//        }
//
//        @Override
//        protected Void doInBackground(File... files) {
//
//            File file = files[0];
//            Log.e(TAG, "Async Image: " + mPhoto.getName());
//            String user_type = user.get(SessionUserData.KEY_USER_TYPE);
//            String id = user.get(SessionUserData.KEY_USER_ID);
//            String group = user.get(SessionUserData.KEY_USER_GROUP);
//            String authentication_key = user.get(SessionUserData.KEY_USER_AUTH_KEY);
//
//            //Lets pass the desired parameters
//            HashMap<String, String> map = new HashMap<String, String>();
//
//            client = new OkHttpClient();
////        MediaType mediaType = MediaType.parse()
//            RequestBody requestBody = new MultipartBuilder()
//                    .type(MultipartBuilder.FORM)
//                    .addFormDataPart("authentication_key", authentication_key)
//                    .addFormDataPart("agent_id", id)
//                    .addFormDataPart("group", group)
//                    .addFormDataPart("filename", file.getName(),
//                            RequestBody.create(MediaType.parse("jpeg"), file))
//                    .build();
//            Log.e(TAG, id+" "+group+" "+authentication_key+" "+file.getName());
//            String serverURL = "";
//            serverURL = ApiParams.TAG_BASE_URL + "/api/"+ApiParams.TAG_PROFILE_UPDATE_KEY;
//            Log.e("Server Url", serverURL);
//            Request request = new Request.Builder()
//                    .url(serverURL)
//                    .post(requestBody)
//                    .build();
//            client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
//                @Override
//                public void onFailure(Request request, IOException e) {
//                    Log.e(TAG, "Failed");
//                }
//                @Override
//                public void onResponse(com.squareup.okhttp.Response response) throws IOException {
//                    Log.e(TAG, response.message());
//                }
//            });
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            hideProgressDialog();
//        }
//    }

    private class GetImages extends AsyncTask<Void, Void, Bitmap>
    {
        @Override
        protected Bitmap doInBackground(Void... params)
        {
            image_drawable.clear();
            for(int i=0; i<image_list.size(); i++)
            {
                mBitmap = rotateBitmapOrientation(path);
//                bitmap = BitmapFactory.decodeFile(image_list.get(i).toString().trim());
//                bitmap = Bitmap.createScaledBitmap(bitmap,500, 500, true);
                return mBitmap;
            }
            return null;
        }

        protected void onPostExecute(Bitmap bitmap)
        {
            imgProfilePic.setImageBitmap(bitmap);
            sendFile(bitmapToFile(getResizedBitmap(bitmap, 50)));
        }
    }

    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public void verifyCameraPermission(Activity activity){
        int permission = ActivityCompat.checkSelfPermission(
                activity,
                Manifest.permission.CAMERA);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_CAMERA,
                    MY_PERMISSIONS_REQUEST_CAMERA);
        }
    }

    private void verifyReadStoragePermissions(Activity activity) {
        int permission = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            permission = ActivityCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_READ_STORAGE,
                    REQUEST_READ_EXTERNAL_STORAGE);
        }
    }

    public Drawable loadImagefromurl(Bitmap icon)
    {
        Drawable d=new BitmapDrawable(icon);
        return d;
    }

    private HashMap<String, String> loadUserDataFromSession(String bloodGroup, String nID, String dOB){
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(ApiParams.TAG_BASE_URL).build();
        myAgentEditProfile = restAdapter.create(AgentEditProfile.class);
        // get user data from session
        user_type = user.get(SessionUserData.KEY_USER_TYPE);
        id = user.get(SessionUserData.KEY_USER_ID);
        group = user.get(SessionUserData.KEY_USER_GROUP);
        authentication_key = user.get(SessionUserData.KEY_USER_AUTH_KEY);

        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();

        if (user_type.equals("1")) {
            map.put(ApiParams.PARAM_ADMIN_ID, "" + id);
        } else if (user_type.equals("2")) {
            map.put(ApiParams.PARAM_AGENT_ID, "" + id);
        }
        map.put(ApiParams.PARAM_GROUP, group);
        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, authentication_key);
        map.put(ApiParams.PARAM_BLOOD_GROUP, ""+bloodGroup);
        map.put(ApiParams.PARAM_NID, ""+nID);
        map.put(ApiParams.PARAM_DOB, ""+dOB);

        return map;
    }

    private boolean callForUpdate( HashMap<String, String> map ){
        returnType = false;
        myAgentEditProfile.getResult(ApiParams.TAG_AGENT_PROFLE_UPDATE_KEY, map, new Callback<Status>() {
            @Override
            public void success(Status model, Response response) {

                String status = model.getStatus();

                if (status.equals(ApiParams.TAG_SUCCESS)) {
                    showToast(getString(R.string.update_success), 0);
                }
                else
                    showErrorToast(model.getMsg(), 0);

                returnType = true;
            }

            @Override
            public void failure(RetrofitError error) {
                showErrorToast(getString(R.string.error_failed), 0);
                returnType = false;
            }
        });
        return returnType;
    }

    private void initDrawer(Bundle savedInstanceState) {
        mContext = ActivityProfile.this;
        mActivity = ActivityProfile.this;
        Log.e(TAG, user_type+ " "+ user_type_name);
        setProfile(MainActivity.user_name, MainActivity.user_type_name, MainActivity.user_pro_pic);
        getAnnouncementNumber(savedInstanceState);
    }

    private void getAnnouncementNumber(final Bundle savedInstanceState) {
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(ApiParams.TAG_BASE_URL).build();
        //creating a service for adapter with our ApiCallback class
        AnnouncementListInterface myApiCallback = restAdapter.create(AnnouncementListInterface.class);

        // get user data from session

        String user_type = user.get(SessionUserData.KEY_USER_TYPE);
        String id = user.get(SessionUserData.KEY_USER_ID);
        String group = user.get(SessionUserData.KEY_USER_GROUP);
        String authentication_key = user.get(SessionUserData.KEY_USER_AUTH_KEY);
        Log.e(TAG, user_type+" "+id+" "+group+" "+authentication_key);

        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(ApiParams.PARAM_AGENT_ID, id);
        map.put(ApiParams.PARAM_GROUP, group);
        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, authentication_key);
        map.put(ApiParams.PARAM_ANNOUNCEMENT_ID, "1");
        myApiCallback.getData(ApiParams.TAG_ANNOUNCEMENT_KEY, map, new Callback<AnnouncementList>() {
            @Override
            public void success(AnnouncementList announcementList, Response response) {
                nAnnouncement = announcementList.getNo_of_items();
                setDrawer(savedInstanceState, mActivity, true, getString(R.string.nav_item_profile), MainActivity.activity_profile, nAnnouncement);
            }

            @Override
            public void failure(RetrofitError error) {
                nAnnouncement = "0";
                setDrawer(savedInstanceState, mActivity, true, getString(R.string.nav_item_profile), MainActivity.activity_profile, nAnnouncement);
            }
        });
    }

    public void setDrawer(Bundle savedInstanceState, Activity activity, boolean isHome, String title,
                          int position, String no_announcement) {

        mContext = activity.getApplicationContext();
        mActivity = activity;
        sessionUserData = new SessionUserData(mContext);
        // Handle Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(isHome);
        getSupportActionBar().setTitle(title);
        buildHeader(savedInstanceState, mActivity);

        result = new DrawerBuilder()
                .withActivity(mActivity)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withDrawerLayout(R.layout.crossfade_drawer)
                .withDrawerWidthDp(72)
                .withGenerateMiniDrawer(true)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
//                        0
                        new PrimaryDrawerItem().withName(R.string.title_home)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_home)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(0),
//                        1
                        new PrimaryDrawerItem().withName(R.string.title_consignment_list)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_list_alt)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(1),
//                        2
                        new PrimaryDrawerItem().withName(R.string.title_reports)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_file_text)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(2),
//                        3
                        new PrimaryDrawerItem().withName(R.string.title_profile)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_user)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(3)
                                .withEnabled(false),
//                        4
                        new PrimaryDrawerItem().withName(R.string.nav_item_announcement)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_bullhorn)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withBadge(no_announcement)
                                .withBadgeStyle(new BadgeStyle(Color.RED, Color.RED))
                                .withIdentifier(4),
//                        5
                        new PrimaryDrawerItem().withName(R.string.nav_item_check_update)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_globe)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(5),
//                        6
                        new PrimaryDrawerItem().withName(R.string.nav_item_attendence)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_check)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(6),
//                        7
                        new PrimaryDrawerItem().withName(R.string.nav_item_logout)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_sign_out)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(7)
                ) // add the items we want to use with our Drawer
                .withOnDrawerNavigationListener(new Drawer.OnDrawerNavigationListener() {
                    @Override
                    public boolean onNavigationClickListener(View clickedView) {
                        //this method is only called if the Arrow icon is shown. The hamburger is automatically managed by the MaterialDrawer
                        //if the back arrow is shown. close the activity
//                        mActivity.finish();
                        //return true if we have consumed the event
                        return true;
                    }
                })
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
//                        Toast.makeText(MapsActivity.this, "onDrawerOpened", Toast.LENGTH_SHORT).show();
                        KeyboardUtil.hideKeyboard(mActivity);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
//                        Toast.makeText(MapsActivity.this, "onDrawerClosed", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem instanceof Nameable) {
                            Toast.makeText(mContext, ((Nameable) drawerItem).getName()
                                    .getText(mContext), Toast.LENGTH_SHORT).show();
//                            0
                            if (drawerItem.getIdentifier() == 0) {
                                startActivity(new Intent(mContext, MainActivity.class));
                            }
//                            1
                            else if (drawerItem.getIdentifier() == 1) {
                                startActivity(new Intent(mContext, ActivityConsignmentList.class));
                            }
//                            2
                            else if (drawerItem.getIdentifier() == 2) {
                                startActivity(new Intent(mContext, ActivityReports.class));
                            }
//                            3
                            else if (drawerItem.getIdentifier() == 3) {
                                startActivity(new Intent(mContext, ActivityProfile.class));
                            }
//                            4
                            else if (drawerItem.getIdentifier() == 4) {
                                startActivity(new Intent(mContext, AnnouncementActivity.class));
                            }
//                            5
                            else if (drawerItem.getIdentifier() == 5) {
                                ActivityProfile.GetVersionCode getVersionCode = new ActivityProfile.GetVersionCode();
                                showProgressDialog(true, " Version Checking", "Please wait...");
                                getVersionCode.execute();
                            }
//                            6
                            else if (drawerItem.getIdentifier() == 6) {
                                startActivity(new Intent(mContext, AttendenceActivity.class));
                            }
//                            7
                            else if (drawerItem.getIdentifier() == 7) {
                                sessionUserData.endSession();
                                LoginActivity.deleteCache(mContext);
                                sessionUserData.checkLogin();
                                finish();
                            }
//                            10
                            else if (drawerItem.getIdentifier() == 10) {
                                showToast("Settings", Toast.LENGTH_SHORT, 0);
//                                startActivity(new Intent(context, SettingsActivity.class));
                            }
                        }
                        return false;
                    }
                })
                .addStickyDrawerItems(
//                        10
                        new PrimaryDrawerItem().withName(R.string.drawer_item_settings)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_cog)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(10)
                )
                .withSelectedItem(position)
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .build();

        crossfadeDrawerLayout = (CrossfadeDrawerLayout) result.getDrawerLayout();
        crossfadeDrawerLayout.setMaxWidthPx(DrawerUIUtils.getOptimalDrawerWidth(this));
        miniResult = result.getMiniDrawer();
        View view = miniResult.build(this);
        view.setBackgroundColor(
                UIUtils.getThemeColorFromAttrOrRes(
                        this,
                        com.mikepenz.materialdrawer.R.attr.material_drawer_background,
                        com.mikepenz.materialdrawer.R.color.material_drawer_background));
        crossfadeDrawerLayout.getSmallView().addView(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        miniResult.withCrossFader(new ICrossfader() {
            @Override
            public void crossfade() {
                boolean isFaded = isCrossfaded();
                crossfadeDrawerLayout.crossfade(400);

                //only close the drawer if we were already faded and want to close it now
                if (isFaded) {
                    result.getDrawerLayout().closeDrawer(GravityCompat.START);
                }
            }

            @Override
            public boolean isCrossfaded() {
                return crossfadeDrawerLayout.isCrossfaded();
            }
        });

    }

    private void buildHeader(Bundle savedInstanceState, Activity activity) {
        final ImagePopup imagePopup;imagePopup = new ImagePopup(this);
        imagePopup.setWindowWidth(800);
        imagePopup.setWindowHeight(800);
        imagePopup.setHideCloseIcon(true);
        imagePopup.setImageOnClickClose(true);
        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(activity)
                .withHeaderBackground(R.drawable.bg_nav_main)
                .addProfiles(profile)
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        String uName = profile.getName().getText();
//                        if (user_name.equals(uName))
//                            startActivity(new Intent(mContext, ActivityProfile.class));

                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        String uName = profile.getName().getText();
                        if (user_name.equals(uName)){
                            if (IMG_DRAWABLE != null)
                                imagePopup.initiatePopup(IMG_DRAWABLE);
//                            Bitmap bitmap = showProfileImageChangeDialog();
//
//                            if (bitmap != null){
//                                Log.e(TAG, bitmap.toString());
//                                mProfile = new ProfileDrawerItem().withName(user_name).withEmail(user_type).withIcon(bitmap);
//                                headerResult.updateProfile(mProfile);
//                            }
                        }
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();
    }

    public void setProfile(String name, String type, String pro_pic_url) {
        Log.e(TAG, pro_pic_url);
        if(pro_pic_url != null && !pro_pic_url.isEmpty() && !pro_pic_url.equals(""))
            profile = new ProfileDrawerItem().withName(name).withEmail(type).withIcon(pro_pic_url);

        else profile = new ProfileDrawerItem().withName(name).withEmail(type)
                .withIcon(getResources().getDrawable(R.drawable.ic_default_propic));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //add the values which need to be saved from the drawer to the bundle
        outState = result.saveInstanceState(outState);
        //add the values which need to be saved from the accountHeader to the bundle
        outState = headerResult.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    public class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + MainActivity.packageName + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (!MainActivity.currentVersion.equals(onlineVersion)) {
                    //show dialog
                    hasNew = true;
                    Log.e("Has New ", " "+hasNew);
                    startActivity(new Intent(mContext, ActivityCheckVersion.class));
                }
            }
            Log.e("update", "Current version " + MainActivity.currentVersion + "playstore version " + onlineVersion+" hasNext : "+hasNew);
            hideProgressDialog();
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public File bitmapToFile(Bitmap bitmap){
        try {
            mPhoto.createNewFile();

    //Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

    //write the bytes in file
            FileOutputStream fos = new FileOutputStream(mPhoto);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mPhoto;
    }
}
