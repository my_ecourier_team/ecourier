/*
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */

package com.ps.ecourier.activities.secondLayer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.hsalf.smilerating.BaseRating;
import com.hsalf.smilerating.SmileRating;
import com.mikepenz.crossfadedrawerlayout.view.CrossfadeDrawerLayout;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.MiniDrawer;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.interfaces.ICrossfader;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;
import com.mikepenz.materialdrawer.util.KeyboardUtil;
import com.mikepenz.materialize.util.UIUtils;
import com.ps.ecourier.R;
import com.ps.ecourier.activities.LoginActivity;
import com.ps.ecourier.activities.MainActivity;
import com.ps.ecourier.activities.firstLayer.ActivityCheckVersion;
import com.ps.ecourier.activities.firstLayer.ActivityConsignmentList;
import com.ps.ecourier.activities.firstLayer.ActivityProfile;
import com.ps.ecourier.activities.firstLayer.ActivityReports;
import com.ps.ecourier.activities.firstLayer.AnnouncementActivity;
import com.ps.ecourier.activities.firstLayer.AttendenceActivity;
import com.ps.ecourier.adapter.ConsignmentSearchSpinnerAdapter;
import com.ps.ecourier.base.BaseActivity;
import com.ps.ecourier.interfaces.ConsignmentDetails;
import com.ps.ecourier.interfaces.TestListInterface;
import com.ps.ecourier.pojo.AnnouncementList;
import com.ps.ecourier.pojo.ConsignmentListDatum;
import com.ps.ecourier.pojo.Status;
import com.ps.ecourier.session.SessionUserData;
import com.ps.ecourier.utils.InputFilterMinMax;
import com.ps.ecourier.webservices.ApiParams;
import com.ps.ecourier.webservices.interfaces.AnnouncementListInterface;
import com.ps.ecourier.webservices.interfaces.ParcelUpdateInterface;
import com.ps.ecourier.webservices.interfaces.ReviewInterface;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import org.jsoup.Jsoup;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.ps.ecourier.activities.MainActivity.user;

public class ActivityConsignmentDetails extends BaseActivity implements ConsignmentDetails {

    public static String CONSIGNMENT_ID;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private Resources res;
    private Button mUpdate;
    private LinearLayout mLayoutConsignmentInformation, mLayoutParcelInformation;
    private RelativeLayout altPhoneNumberLayout;
    private Spinner statusSpinner, commentSpinner;
//    private CustomToast customToast;
    private TextView mConsignmentId, mSenderGroup, mCompany, mCompanyPhone, editComment, mCallCompany,
        mProductId, mOrderTime, mDeliveryTime, mParcelStatus, mCallRecipient,mCallRecipientAlt, mParcelStatusReason,
        mItemType;
    private EditText editInput, etItem, etCollectedAmount, editProductPrice, editRecipientName, editRecipientMobile,editRecipientMobileAlt, editRecipientAddress;
    private List<String> parcelStatusValuesArray;
    private ArrayList<ConsignmentListDatum> myList;
    private int position;
    private int spinner_position = 0;
    private String statusParcel = "",commentParcel = "", consignment_no = "", mCollected_amount = "", mPaymentMethod = "";
    private String current_parcel_status = "", mItems ="", alter_mobile = "",  parcelStatusByName = "";
    private float ratingOfServiceReview = 0,ratingOfProductReview =0;
    final private String TAG = "Consignment Details";
    private Context context;
    private Bitmap signature;
    private SmileRating ratingBarServiceReview, ratingBarProductReview;
    private File photo;
    private SignaturePad mSignaturePad;
    private Intent homeActivity;
    private AlertDialog b;
    private ProgressDialog progressDialog;
    private TestListInterface myApiCallback;
    private ParcelUpdateInterface myParcelUpdateInterface;
    private ReviewInterface reviewInterface;
    private String user_type = "", id = "", group = "", authentication_key = "";
    private boolean returnType = false;
    private String current_parcel_status_code = "";
    private String where_from = "";
    private ConsignmentListDatum datum = null;
    //    drawer setup
    private AccountHeader headerResult = null;
    private Drawer result = null;
    private CrossfadeDrawerLayout crossfadeDrawerLayout = null;
    private IProfile profile;
    private MiniDrawer miniResult = null;
    private Context mContext;
    private Activity mActivity;
    private SessionUserData sessionUserData;
    private boolean hasNew = false;
    private boolean isCollectedAmountEnabled, isCollectedItemEnabled;
    private String nAnnouncement;
    private String[] commentKeyArray;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    showErrorToast("Cannot write images to external storage", Toast.LENGTH_SHORT);
//                    Toast.makeText(context, "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        verifyStoragePermissions(ActivityConsignmentDetails.this);
        setContentView(R.layout.activity_consignment_details);
        initDrawer(savedInstanceState);
        context = ActivityConsignmentDetails.this;
        homeActivity = new Intent(context, MainActivity.class);
        res = getResources();
        loadUserDataFromSession();
        initializeViews();

        parcelStatusValuesArray = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.parcelStatusValuesArray1)));

        // receives data from intent and and serialize it back to a list
        if (getIntent().getExtras() != null) {
            where_from = getIntent().getStringExtra("where_from");
            if(where_from != null && !where_from.isEmpty()){
                if (where_from.equals("list")){
                    datum =(ConsignmentListDatum) getIntent().getSerializableExtra("consignment_data");
                    setConsignmentDetails(datum);
                }
                else if (where_from.equals("search")){
                    position = Integer.parseInt(getIntent().getStringExtra("consignment_data_position"));
                    myList = (ArrayList<ConsignmentListDatum>) getIntent().getSerializableExtra("consignment_data_array");
                    datum = myList.get(position);
                    setConsignmentDetails(datum);
                }
            }
        }

        if (Integer.parseInt(getIntent().getStringExtra("visible_state")) == 0) {
//            no need to hide percel info
//            mLayoutParcelInformation.setVisibility(View.GONE);
        }

        // agent can change status to 1. Picked Up 2. Delivered 3. Partial Delivered 4. HOLD at ECOURIER HUB only
//
        // check the status sequence and verify if the next status is changeable
        if (current_parcel_status_code.equals(res.getString(R.string.s10))
                || current_parcel_status_code.equals(res.getString(R.string.s21))
                || current_parcel_status_code.equals(res.getString(R.string.s25))) {
            if (current_parcel_status_code.equals(res.getString(R.string.s10))) {
                parcelStatusValuesArray = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.parcelStatusValuesArray1)));
            }
            else if (current_parcel_status_code.equals(res.getString(R.string.s21))) {
                parcelStatusValuesArray = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.parcelStatusValuesArray2)));
            }
            else if (current_parcel_status_code.equals(res.getString(R.string.s25))) {
                parcelStatusValuesArray = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.parcelStatusValuesArray5)));
            }
        }
        updateUI(current_parcel_status_code);
        // convert list back to array
        String parcelStatusValues[] = new String[parcelStatusValuesArray.size()];
        parcelStatusValues = parcelStatusValuesArray.toArray(parcelStatusValues);

        statusSpinner.setAdapter(new ConsignmentSearchSpinnerAdapter(this, R.layout.spinner_item, parcelStatusValues));
        final String[] finalParcelStatusValues = parcelStatusValues;
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinner_position = position;
                if (position != 0) {
                    parcelStatusByName = finalParcelStatusValues[position];
                    statusParcel = updateParcelStatus(parcelStatusByName);
                } else {
                    parcelStatusByName = finalParcelStatusValues[0];
                    statusParcel = updateParcelStatus(parcelStatusByName);
                }
                //Delivered -> on the way to delivery
                // Picked Up -> initiated
                Log.e("parcelStatusByName: ", parcelStatusByName);
//                updateUI(status);
                if(statusParcel.equals("S5")||statusParcel.equals("S7")){
                    commentSpinner.setVisibility(View.VISIBLE); //changed 1.0.7
                    commentSpinner.setEnabled(true);
                    commentKeyArray = getResources().getStringArray(R.array.parcelCommentKeysArray);
                    commentSpinner.setAdapter(new ConsignmentSearchSpinnerAdapter(ActivityConsignmentDetails.this,
                            R.layout.spinner_item, getResources().getStringArray(R.array.parcelCommentKeysArray)));
                }
                else if (statusParcel.equals(getResources().getString(R.string.s4))){
                    commentSpinner.setVisibility(View.VISIBLE); //changed 1.0.7
                    commentSpinner.setEnabled(true);
                    commentKeyArray = getResources().getStringArray(R.array.parcelCommentKeysArrayDelivered);
                    commentSpinner.setAdapter(new ConsignmentSearchSpinnerAdapter(ActivityConsignmentDetails.this,
                            R.layout.spinner_item, getResources().getStringArray(R.array.parcelCommentKeysArrayDelivered)));
                }
                else {
                    commentSpinner.setVisibility(View.GONE);
                    commentSpinner.setEnabled(false);
                    editComment.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                parcelStatusByName = finalParcelStatusValues[0];
                statusParcel = updateParcelStatus(parcelStatusByName);
            }
        });
        commentKeyArray = getResources().getStringArray(R.array.parcelCommentKeysArrayDelivered);
        commentSpinner.setAdapter(new ConsignmentSearchSpinnerAdapter(this, R.layout.spinner_item, commentKeyArray));
        commentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                commentParcel = "" + commentKeyArray[position];

                if (commentParcel.equals("Other")) {
                    editComment.setVisibility(View.VISIBLE);
                } else {
                    editComment.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                String commentArray[] = getResources().getStringArray(R.array.parcelCommentKeysArray);
                commentParcel = commentArray[0];

                if (commentParcel.equals("Other")) {
                    editComment.setVisibility(View.VISIBLE);
                } else {
                    editComment.setVisibility(View.GONE);
                }
            }
        });

        mCallCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ConsignmentListDatum data = datum;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel: " + data.getEsoMobile()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mCallRecipient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ConsignmentListDatum data = datum;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel: " + data.getRecipientMobile()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mCallRecipientAlt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ConsignmentListDatum data = datum;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel: " + data.getAlter_mobile()));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mUpdate.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {

                // agent can change status to 1. Picked Up 2. Delivered 3. Partial Delivered 4. HOLD at ECOURIER HUB only
                // check the status sequence and verify if the next status is changeable
//                On the way to Delivery
                String s = mUpdate.getText().toString();
                Log.e("button text ",s);
                Log.e("current_parcel_status mUpdate onClick: ", current_parcel_status_code);
                if (current_parcel_status_code.equals(res.getString(R.string.s21))
                        || current_parcel_status_code.equals(res.getString(R.string.s25))) {
                    if (commentSpinner.getSelectedItem().toString().equals("Other")) {
                        commentParcel = editComment.getText().toString();
                        if (commentParcel.equals(""))
                            commentParcel = "No Comment Available";
                    }
                    else if (commentSpinner.getSelectedItem().toString().equals("Product problem"))
                        commentParcel = "No Comment Available";
                    else
                        commentParcel = commentSpinner.getSelectedItem().toString();

                    updateParcel();
                }
                else if (current_parcel_status_code.equals(res.getString(R.string.s10))){
                    commentParcel = "Picked";
                    updateParcel();
                }
                else {
                    startViewAccordingToPrefs(MainActivity.class);
                }
            }
        });
    }

    private void startViewAccordingToPrefs(Class<?> activityClass){
        Log.e(TAG, where_from);
        if (where_from.equals("list")){
            Intent intent = new Intent(ActivityConsignmentDetails.this,activityClass);
            intent.putExtra("fragmentNumber",2);
            startActivity(intent);
        }
        else if (where_from.equals("search")){
            Intent intent = new Intent(ActivityConsignmentDetails.this,activityClass);
            intent.putExtra("fragmentNumber",1);
            startActivity(intent);
        }
    }

    private void updateUI(String current_parcel_status_code) {
//            #2 initiated
        if(current_parcel_status_code.equals(res.getString(R.string.s10))){
            statusSpinner.setVisibility(View.VISIBLE); //changed 1.0.7
            statusSpinner.setEnabled(true);
            commentSpinner.setVisibility(View.GONE); //changed 1.0.7
            commentSpinner.setEnabled(false);
            mUpdate.setText(getResources().getString(R.string.button_update));
//            mUpdate.setClickable(true);
        }
//            #3 on the way to delivery
        else if(current_parcel_status_code.equals(res.getString(R.string.s21))){
            statusSpinner.setVisibility(View.VISIBLE); //changed 1.0.7
            statusSpinner.setEnabled(true);
            commentSpinner.setVisibility(View.VISIBLE); //changed 1.0.7
            commentSpinner.setEnabled(true);
            mUpdate.setText(getResources().getString(R.string.button_update));
//            mUpdate.setClickable(true);
        }
        else if(current_parcel_status_code.equals(res.getString(R.string.s25))){
            statusSpinner.setVisibility(View.VISIBLE); //changed 1.0.7
            statusSpinner.setEnabled(true);
            mUpdate.setText(getResources().getString(R.string.button_update));
//            mUpdate.setClickable(true);
        }
        else {
            statusSpinner.setVisibility(View.GONE); //changed 1.0.7
            commentSpinner.setVisibility(View.GONE); //changed 1.0.7
            statusSpinner.setEnabled(false);
            commentSpinner.setEnabled(false);
            mUpdate.setText("BACK");
            showErrorToast("You aren't allowed to change the status right now!", 0);
        }
    }

    private String updateParcelStatus(String status) {
        String parcelStatus;
        if(status.equals("Picked Up"))
            parcelStatus="S2";
        else if(status.equals("Delivered"))
            parcelStatus="S4";
        else if(status.equals("Partial Delivered"))
            parcelStatus="S5";
        else if(status.equals("Hold at eCourier Hub"))
            parcelStatus="S7";
        else if(status.equals("Parcel Return"))
            parcelStatus="S24";
        else parcelStatus = status;
        //s4 -> on the way to delivery
//        s2 -> initiated
        Log.e("parcelStatus", parcelStatus);
        return parcelStatus;

    }

    private void initializeViews() {
        editComment = (TextView) findViewById(R.id.editComment);
        mUpdate = (Button) findViewById(R.id.btnUpdate);
        mLayoutConsignmentInformation = (LinearLayout) findViewById(R.id.layoutConsignmentInformation);
        mLayoutParcelInformation = (LinearLayout) findViewById(R.id.layoutParcelInformation);
        mConsignmentId = (TextView) findViewById(R.id.textConsignmentId);
        mSenderGroup = (TextView) findViewById(R.id.textSenderGroup);
        mCompany = (TextView) findViewById(R.id.textCompany);
        mCompanyPhone = (TextView) findViewById(R.id.textCompanyMobile);
        mCallCompany = (TextView) findViewById(R.id.txtCompanyMobile);
        mProductId = (TextView) findViewById(R.id.textProductId);
        mCallRecipient = (TextView) findViewById(R.id.txtRecipientMobile);
        mCallRecipientAlt = (TextView) findViewById(R.id.txtRecipientMobileAlt);
        editProductPrice = (EditText) findViewById(R.id.editProductPrice);
        editRecipientName = (EditText) findViewById(R.id.editRecipientName);
        editRecipientMobile = (EditText) findViewById(R.id.editRecipientMobile);
        editRecipientMobileAlt = (EditText) findViewById(R.id.editRecipientMobileAlt);
        editRecipientAddress = (EditText) findViewById(R.id.editRecipientAddress);
        mOrderTime = (TextView) findViewById(R.id.textOrderTime);
        mDeliveryTime = (TextView) findViewById(R.id.textActualDeliveryTime);
        mParcelStatus = (TextView) findViewById(R.id.textParcelStatus);
        mParcelStatusReason = (TextView) findViewById(R.id.textParcelStatusReason);
        statusSpinner = (Spinner) findViewById(R.id.spinnerStatusKey);
        commentSpinner = (Spinner) findViewById(R.id.spinnerCommentKey);
        altPhoneNumberLayout =  (RelativeLayout) findViewById(R.id.altPhoneNumberLayout);
        mItemType = (TextView) findViewById(R.id.tvItemType);

        editComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentParcel = promptComment();
            }
        });
    }

    @Override
    public void setConsignmentDetails(List<ConsignmentListDatum> consignmentListDatums, int position) {
        ConsignmentListDatum data = consignmentListDatums.get(position);

        if (getIntent().getBooleanExtra("is_search", false)) {
            consignment_no = data.getConsignmentNo();
            mCompany.setText(String.format(res.getString(R.string.company), data.getEso()));
//            mCompanyPhone.setText(String.format(res.getString(R.string.contact), data.getEsoMobile()));  //changed 1.0.6
            mCompanyPhone.setVisibility(View.GONE); //changed 1.0.6
        } else {
            consignment_no = data.getConsignmentId();
            mCompany.setText(String.format(res.getString(R.string.company), data.getCompany()));
            mCompanyPhone.setVisibility(View.GONE);
//            mCallCompany.setVisibility(View.GONE);
        }
        mPaymentMethod = data.getPaymentMethod();
        mCollected_amount = data.getProductPrice();
        mItems = data.getItems();
        alter_mobile = data.getAlter_mobile();
        if (data.getItemType() != null && !data.getItemType().isEmpty()) {
            mItemType.setText(data.getItemType());
        }
        if (alter_mobile != null && !alter_mobile.equals("") && !alter_mobile.isEmpty()){
            altPhoneNumberLayout.setVisibility(View.VISIBLE);
            editRecipientMobileAlt.setVisibility(View.VISIBLE);
            mCallRecipientAlt.setVisibility(View.VISIBLE);
            editRecipientMobileAlt.setText(alter_mobile);
        }else {
            altPhoneNumberLayout.setVisibility(View.GONE);
            editRecipientMobileAlt.setVisibility(View.GONE);
            mCallRecipientAlt.setVisibility(View.GONE);
        }
        CONSIGNMENT_ID = consignment_no;
        mConsignmentId.setText(String.format(res.getString(R.string.consignment_id), consignment_no));
        mSenderGroup.setText(String.format(res.getString(R.string.sender_group), data.getSenderGroup()));
        mProductId.setText(String.format(res.getString(R.string.product_id), data.getProductId()));
        editProductPrice.setText("" + data.getProductPrice());
        editRecipientName.setText("" + data.getRecipientName());
        editRecipientMobile.setText("" + data.getRecipientMobile());
        editRecipientAddress.setText("" + data.getRecipientAddress());
        mOrderTime.setText(String.format(res.getString(R.string.order_time), data.getOrderTime()));
        mParcelStatus.setText(String.format(res.getString(R.string.parcel_status), data.getParcelStatus()));
        current_parcel_status_code = data.getStatusCode();
        current_parcel_status = data.getParcelStatus();
        if (!data.getParcelStatus().equals(getString(R.string.spinner_consignment_type_init))){
            mDeliveryTime.setVisibility(View.VISIBLE);
            mDeliveryTime.setText(String.format(res.getString(R.string.delivery_time), data.getActualDeliveryTime()));
        }

    }

    public void setConsignmentDetails(ConsignmentListDatum data) {

        if (getIntent().getBooleanExtra("is_search", false)) {
            consignment_no = data.getConsignmentNo();
            mCompany.setText(String.format(res.getString(R.string.company), data.getEso()));
//            mCompanyPhone.setText(String.format(res.getString(R.string.contact), data.getEsoMobile()));  //changed 1.0.6
            mCompanyPhone.setVisibility(View.GONE); //changed 1.0.6
        } else {
            consignment_no = data.getConsignmentId();
            mCompany.setText(String.format(res.getString(R.string.company), data.getCompany()));
            mCompanyPhone.setVisibility(View.GONE);
//            mCallCompany.setVisibility(View.GONE);
        }
        mPaymentMethod = data.getPaymentMethod();
        mCollected_amount = data.getProductPrice();
        mItems = data.getItems();
        alter_mobile = data.getAlter_mobile();
        if (data.getItemType() != null && !data.getItemType().isEmpty()) {
            mItemType.setText(data.getItemType());
        }
        if (alter_mobile != null && !alter_mobile.equals("") && !alter_mobile.isEmpty()){
            altPhoneNumberLayout.setVisibility(View.VISIBLE);
            editRecipientMobileAlt.setVisibility(View.VISIBLE);
            mCallRecipientAlt.setVisibility(View.VISIBLE);
            editRecipientMobileAlt.setText(alter_mobile);
        }else {
            altPhoneNumberLayout.setVisibility(View.GONE);
            editRecipientMobileAlt.setVisibility(View.GONE);
            mCallRecipientAlt.setVisibility(View.GONE);
        }
        CONSIGNMENT_ID = consignment_no;
        mConsignmentId.setText(String.format(res.getString(R.string.consignment_id), consignment_no));
        mSenderGroup.setText(String.format(res.getString(R.string.sender_group), data.getSenderGroup()));
        mProductId.setText(String.format(res.getString(R.string.product_id), data.getProductId()));
        editProductPrice.setText("" + data.getProductPrice());
        editRecipientName.setText("" + data.getRecipientName());
        editRecipientMobile.setText("" + data.getRecipientMobile());
        editRecipientAddress.setText("" + data.getRecipientAddress());
        mOrderTime.setText(String.format(res.getString(R.string.order_time), data.getOrderTime()));
        mParcelStatus.setText(String.format(res.getString(R.string.parcel_status), data.getParcelStatus()));
        current_parcel_status_code = data.getStatusCode();
        current_parcel_status = data.getParcelStatus();
        if (!data.getParcelStatus().equals(getString(R.string.spinner_consignment_type_init))){
            mDeliveryTime.setVisibility(View.VISIBLE);
            mDeliveryTime.setText(String.format(res.getString(R.string.delivery_time), data.getActualDeliveryTime()));
        }

    }


    @SuppressLint("LongLogTag")
    private void updateParcel() {
//        showProgressDialog(false, "", getResources().getString(R.string.loading));
        // get user data from session
        String user_type = MainActivity.user.get(SessionUserData.KEY_USER_TYPE);
        String id = MainActivity.user.get(SessionUserData.KEY_USER_ID);
        String group = MainActivity.user.get(SessionUserData.KEY_USER_GROUP);
        String authentication_key = MainActivity.user.get(SessionUserData.KEY_USER_AUTH_KEY);

        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();

        if (user_type.equals("1")) {
            map.put(ApiParams.PARAM_ADMIN_ID, "" + id);
        } else if (user_type.equals("2")) {
            map.put(ApiParams.PARAM_AGENT_ID, "" + id);
        }

        statusParcel = updateParcelStatus(statusSpinner.getSelectedItem().toString());

        map.put(ApiParams.PARAM_GROUP, group);
        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, authentication_key);
        map.put(ApiParams.TAG_CONSIGNMENT_NO, consignment_no);
        map.put(ApiParams.PARAM_STATUS, statusParcel);
        map.put(ApiParams.PARAM_COLLECTED_AMOUNT, mCollected_amount);
        map.put(ApiParams.PARAM_COLLECTED_ITEMS, mItems);
        map.put(ApiParams.PARAM_POD, "");
        map.put(ApiParams.PARAM_COMMENT, commentParcel);
        isCollectedAmountEnabled = false;
        isCollectedItemEnabled = false;
//        s4 -> delivered
        Log.e(TAG, "statusParcel: " + statusParcel+" comment: "+ commentParcel);

        if(statusParcel.equals("S24")){
            if(callForUpdate(map)){
                showSuccessToast(getString(R.string.update_success), 0);
                startViewAccordingToPrefs(MainActivity.class);
            }
        }
        if (statusParcel.equals("S4")) {
            map.put(ApiParams.PARAM_POD, "Delivered");
        }

        Log.e("<<<Important>>>",statusSpinner.getSelectedItem().toString());
        if (statusSpinner.getSelectedItem().toString().equals("Picked Up")){
            if(callForUpdate(map)){
                showSuccessToast(getString(R.string.update_success), 0);
                startViewAccordingToPrefs(MainActivity.class);
            }
        }
        else if (statusSpinner.getSelectedItem().toString().equals("Delivered")) {
            promptReview(map, statusSpinner.getSelectedItem().toString().toLowerCase());
        }
        else if(statusSpinner.getSelectedItem().toString().equals("Partial Delivered")){
            promptReview(map, statusSpinner.getSelectedItem().toString().toLowerCase());
        }
        else if(statusSpinner.getSelectedItem().toString().equals("Hold at eCourier Hub")){
            if(callForUpdate(map)){
                showSuccessToast(getString(R.string.update_success), 0);
                startViewAccordingToPrefs(MainActivity.class);
            }
        }
    }

    private boolean callForUpdate( HashMap<String, String> map ){
        returnType = true;
        myParcelUpdateInterface.getResult(ApiParams.TAG_PARCEL_UPDATE_KEY, map, new Callback<Status>() {
            @Override
            public void success(Status model, Response response) {
                String status = model.getStatus();
                Log.e(TAG, "callForUpdate :"+status);
                if (status.equals(ApiParams.TAG_SUCCESS)) {}
                returnType = true;
                Log.e(TAG, "R.T 1: "+returnType);
                hideProgressDialog();
            }

            @Override
            public void failure(RetrofitError error) {
//                showErrorToast(getString(R.string.error_failed), 0);
                returnType = false;
                Log.e(TAG, "R.T 2: "+returnType);
                hideProgressDialog();
            }
        });
        Log.e(TAG, "R.T 3: "+returnType);
        return returnType;
    }

    private String promptComment() {
        final String[] comment = {""};
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogViewComment = inflater.inflate(R.layout.dialog_comment, null);
        dialogBuilder.setView(dialogViewComment);
        b = dialogBuilder.create();
        b.setCancelable(true);
        b.show();
        final EditText editInput = (EditText) dialogViewComment.findViewById(R.id.editInput);
        final Button btnClear = (Button) dialogViewComment.findViewById(R.id.btnClearComment);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editInput.setText("");
                editComment.setText("");
            }

        });

        final Button btnOk = (Button) dialogViewComment.findViewById(R.id.btnOkComment);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment[0] = editInput.getText().toString();
                editComment.setText(comment[0]);
                b.cancel();
            }

        });

        final Button btnCancel = (Button) dialogViewComment.findViewById(R.id.btnCancelComment);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editInput.setText("");
                editComment.setText("");
                b.cancel();
            }

        });

        return comment[0];
    }


    @SuppressLint("LongLogTag")
    private void promptReview(final HashMap<String, String> mapPrompt, final String status) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_review, null);
        dialogBuilder.setView(dialogView);
        b = dialogBuilder.create();
        b.setCancelable(true);
        b.show();
        ratingBarServiceReview = (SmileRating) dialogView.findViewById(R.id.ratingBarServiceReview);
        ratingBarProductReview = (SmileRating) dialogView.findViewById(R.id.ratingBarProductReview);
        LinearLayout llCollctedAmmount = (LinearLayout) dialogView.findViewById(R.id.llCollectedAmount);
        LinearLayout llItems = (LinearLayout) dialogView.findViewById(R.id.llItems);
        etCollectedAmount = (EditText) dialogView.findViewById(R.id.etCollectedAmount);
        etItem = (EditText) dialogView.findViewById(R.id.etItems);
        Log.e(TAG, mPaymentMethod);
        if (mPaymentMethod.trim().toLowerCase().equals("cod")){
            if (status.equals(getResources().getString(R.string.spinner_consignment_type_partial).toLowerCase())){
                if(!(mCollected_amount.equals("0")||mCollected_amount.equals(""))) {
                    isCollectedAmountEnabled = true;
                    llCollctedAmmount.setVisibility(View.VISIBLE);
                    etCollectedAmount.setHint(String.format(res.getString(R.string.below), mCollected_amount));
                    etCollectedAmount.setFilters(new InputFilter[]{new InputFilterMinMax("0", mCollected_amount, 1)});
                }
                Log.e(TAG, mItems);
                if(!(mItems.equals("0")||mItems.equals(""))) {
                    isCollectedItemEnabled = true;
                    llItems.setVisibility(View.VISIBLE);
                    etItem.setHint(String.format(res.getString(R.string.below), mItems));
                    etItem.setFilters(new InputFilter[]{new InputFilterMinMax("0", mItems, 1)});
                }
            }
            else if (status.equals(getResources().getString(R.string.spinner_consignment_type_delivery).toLowerCase())){
                if(!(mCollected_amount.equals("0")||mCollected_amount.equals(""))) {
                    isCollectedAmountEnabled = true;
                    llCollctedAmmount.setVisibility(View.VISIBLE);
                    etCollectedAmount.setHint(String.format(res.getString(R.string.below), mCollected_amount));
                    etCollectedAmount.setFilters(new InputFilter[]{new InputFilterMinMax("0", mCollected_amount, 0)});
                }
            }
        }
        else {
            llCollctedAmmount.setVisibility(View.GONE);
            llItems.setVisibility(View.GONE);
        }

        ratingBarServiceReview.setOnSmileySelectionListener(new SmileRating.OnSmileySelectionListener() {
            @Override
            public void onSmileySelected(int smiley) {
                ratingOfServiceReview = (float) smiley;
            }
        });
        ratingBarProductReview.setOnSmileySelectionListener(new SmileRating.OnSmileySelectionListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onSmileySelected(int smiley) {
                ratingOfProductReview = (float)smiley;
            }
        });
        mSignaturePad = (SignaturePad) dialogView.findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {

            @Override
            public void onStartSigning() {
                //Event triggered when the pad is touched
            }

            @Override
            public void onSigned() {
                //Event triggered when the pad is signed
                signature = mSignaturePad.getSignatureBitmap();
            }

            @Override
            public void onClear() {
                //Event triggered when the pad is cleared
            }
        });

        final Button btnCancle = (Button) dialogView.findViewById(R.id.btnCancle);
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDialog();
                b.cancel();
                hideDialog();
            }

        });

        final Button btnClear = (Button) dialogView.findViewById(R.id.clear_button);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDialog();
            }

        });

        final Button btnRate = (Button) dialogView.findViewById(R.id.btnRate);
        btnRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    boolean hasItems = true;
                    boolean hasAmount = true;
                    if (mPaymentMethod.trim().toLowerCase().equals("cod")){
                        if (status.equals(getResources().getString(R.string.spinner_consignment_type_partial).toLowerCase())){
                            if (isCollectedAmountEnabled){
                                if (etCollectedAmount.getText().toString().trim().equals("")){
                                    hasAmount = false;
                                    etCollectedAmount.setError( getString(R.string.required));
                                }
                                else{
                                    hasAmount = true;
                                    mapPrompt.put(ApiParams.PARAM_COLLECTED_AMOUNT, etCollectedAmount.getText().toString());
                                }
                            }
                            if (isCollectedItemEnabled){
                                if (etItem.getText().toString().trim().equals("")){
                                    hasItems = false;
                                    etItem.setError( getString(R.string.required));
                                }
                                else{
                                    hasItems = true;
                                    mapPrompt.put(ApiParams.PARAM_COLLECTED_ITEMS, etItem.getText().toString());
                                }
                            }
                            if (hasAmount && hasItems)
                                callStartSend(mapPrompt);
                        }
                        else if (status.equals(getResources().getString(R.string.spinner_consignment_type_delivery).toLowerCase())){
                            if (isCollectedAmountEnabled){
                                if (etCollectedAmount.getText().toString().equals(""))
                                    mapPrompt.put(ApiParams.PARAM_COLLECTED_AMOUNT, mCollected_amount);
                                else
                                    mapPrompt.put(ApiParams.PARAM_COLLECTED_AMOUNT, etCollectedAmount.getText().toString());

                                callStartSend(mapPrompt);
                            }
                        }
                    }
                    else {
                        callStartSend(mapPrompt);
                    }
                } else {
                    resetDialog();
                }
            }
        });

        dialogBuilder.setTitle("Rate our service:");

    }

    private void callStartSend(HashMap<String, String> mapPrompt) {
        b.dismiss();
        Log.e("amount and items 2: ", mapPrompt.get(ApiParams.PARAM_COLLECTED_AMOUNT) +
                " "+ mapPrompt.get(ApiParams.PARAM_COLLECTED_ITEMS));
        if(callForUpdate(mapPrompt)){
            showSuccessToast(getString(R.string.update_success), 0);
            startViewAccordingToPrefs(MainActivity.class);
        }
        long size = getFolderSize(photo)/1024;
        Log.e("File Size ", ""+size);
        sendFile(""+(ratingOfServiceReview), ""+(ratingOfProductReview), photo);
    }

    private void resetDialog() {
        ratingBarServiceReview.setSelectedSmile(BaseRating.OKAY, true);
        ratingBarProductReview.setSelectedSmile(BaseRating.OKAY, true);
        ratingOfServiceReview = 3;
        ratingOfProductReview = 3;
        mSignaturePad.clear();
        etItem.setText("");
        etCollectedAmount.setText("");
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        file.mkdir();
        if (!file.exists()) {
            Log.e("SignaturePad : ", "External Directory not created");
            file = new File(context.getFilesDir(), albumName);
            file.mkdir();
            if (!(file.exists() && file.isDirectory())){
                Log.e("SignaturePad : ", "Internal Directory not created");
            }
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    public void saveBitmapToPNG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.PNG, 40, stream);
        stream.close();
    }

    public boolean addJpgSignatureToGallery(Bitmap signature) {
        signature = getResizedBitmap(signature, 200);
        boolean result = false;
        try {
            photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    private void loadUserDataFromSession(){
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(ApiParams.TAG_BASE_URL).build();
        myApiCallback = restAdapter.create(TestListInterface.class);
        myParcelUpdateInterface = restAdapter.create(ParcelUpdateInterface.class);
        reviewInterface = restAdapter.create(ReviewInterface.class);
        // get user data from session
        user_type = MainActivity.user.get(SessionUserData.KEY_USER_TYPE);
        id = MainActivity.user.get(SessionUserData.KEY_USER_ID);
        group = MainActivity.user.get(SessionUserData.KEY_USER_GROUP);
        authentication_key = MainActivity.user.get(SessionUserData.KEY_USER_AUTH_KEY);
    }

    private void sendDataTest(String serviceRating, String productRating, File photo){
        showDialog(false, "", getResources().getString(R.string.loading));
        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();

        if (user_type.equals("1")) {
            map.put(ApiParams.PARAM_ADMIN_ID, "" + id);
        } else if (user_type.equals("2")) {
            map.put(ApiParams.PARAM_AGENT_ID, "" + id);
        }
        map.put(ApiParams.PARAM_GROUP, group);
        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, authentication_key);
        map.put(ApiParams.TAG_CONSIGNMENT_ID, consignment_no);
//        map.put(ApiParams.PARAM_AGENT_ID, "12");
//        map.put(ApiParams.PARAM_GROUP, "ECA");
//        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, "162e7035b4a554ad57d82682014f9bea");
//        map.put(ApiParams.TAG_CONSIGNMENT_ID, "ECR0002703170016");
        map.put(ApiParams.PARAM_SERVICE_REVIEW, serviceRating);
        map.put(ApiParams.PARAM_PRODUCT_REVIEW, productRating);

        myApiCallback.getData(ApiParams.TAG_USER_REVIEW_KEY, map, new Callback<Status>() {

            @Override
            public void success(Status status, Response response) {
                hideDialog();
                Log.e("Review Status", status.getMsg()+" "+status.getStatus());
                startViewAccordingToPrefs(MainActivity.class);
            }

            @Override
            public void failure(RetrofitError error) {
                resetDialog();
            }
        });
    }

    @SuppressLint("LongLogTag")
    private void sendFile(String serviceRating, String productRating, File file){
        long size = getFolderSize(file)/1024;

        String user_type = MainActivity.user.get(SessionUserData.KEY_USER_TYPE);
        String id = MainActivity.user.get(SessionUserData.KEY_USER_ID);
        String group = MainActivity.user.get(SessionUserData.KEY_USER_GROUP);
        String authentication_key = MainActivity.user.get(SessionUserData.KEY_USER_AUTH_KEY);

        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();

        OkHttpClient client = new OkHttpClient();
//        MediaType mediaType = MediaType.parse()
        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addFormDataPart("authentication_key", authentication_key)
                .addFormDataPart("agent_id", id)
                .addFormDataPart("group", group)
                .addFormDataPart("consignment_id", consignment_no)
                .addFormDataPart("service_review", serviceRating)
                .addFormDataPart("product_reviw", productRating)
                .addFormDataPart("comment", "Testing...")
                .addFormDataPart("filename", file.getName(),
                        RequestBody.create(MediaType.parse("jpeg"), file))
                .build();
        Log.e(TAG, id+" "+group+" "+consignment_no+" "+serviceRating+" "+productRating+" "+authentication_key+" "+file.getName());
        String serverURL = "http://test.ecourier.com.bd/api/userReview.php";
        serverURL = ApiParams.TAG_BASE_URL + "/api/"+ApiParams.TAG_USER_REVIEW_KEY;
        Request request = new Request.Builder()
                .url(serverURL)
                .post(requestBody)
                .build();
        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                startViewAccordingToPrefs(MainActivity.class);
            }
            @Override
            public void onResponse(com.squareup.okhttp.Response response) throws IOException {
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        showSuccessToast(mActivity, getString(R.string.update_success), 0);
                    }
                });
                startViewAccordingToPrefs(MainActivity.class);
            }
        });

    }

    public static long getFolderSize(File f) {
        long size = 0;
        if (f.isDirectory()) {
            for (File file : f.listFiles()) {
                size += getFolderSize(file);
            }
        } else {
            size=f.length();
        }
        return size;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public void hideDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }

    public void showDialog(boolean show_title, String title, String message) {
        progressDialog = new ProgressDialog(ActivityConsignmentDetails.this);
        if (show_title) {
            progressDialog.setTitle(title);
        }
        progressDialog.setMessage(message);
        progressDialog.show();

    }

    private void initDrawer(Bundle savedInstanceState) {
        mContext = ActivityConsignmentDetails.this;
        mActivity = ActivityConsignmentDetails.this;
        Log.e(TAG, user_type+ " "+ user_type_name);
        setProfile(MainActivity.user_name, MainActivity.user_type_name, MainActivity.user_pro_pic);
        getAnnouncementNumber(savedInstanceState);
    }

    private void getAnnouncementNumber(final Bundle savedInstanceState) {
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(ApiParams.TAG_BASE_URL).build();
        //creating a service for adapter with our ApiCallback class
        AnnouncementListInterface myApiCallback = restAdapter.create(AnnouncementListInterface.class);

        // get user data from session

        String user_type = user.get(SessionUserData.KEY_USER_TYPE);
        String id = user.get(SessionUserData.KEY_USER_ID);
        String group = user.get(SessionUserData.KEY_USER_GROUP);
        String authentication_key = user.get(SessionUserData.KEY_USER_AUTH_KEY);
        Log.e(TAG, user_type+" "+id+" "+group+" "+authentication_key);

        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(ApiParams.PARAM_AGENT_ID, id);
        map.put(ApiParams.PARAM_GROUP, group);
        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, authentication_key);
        map.put(ApiParams.PARAM_ANNOUNCEMENT_ID, "1");
        myApiCallback.getData(ApiParams.TAG_ANNOUNCEMENT_KEY, map, new Callback<AnnouncementList>() {
            @Override
            public void success(AnnouncementList announcementList, Response response) {
                nAnnouncement = announcementList.getNo_of_items();
                setDrawer(savedInstanceState, mActivity, true, getString(R.string.title_consignment_details), MainActivity.activity_cn, nAnnouncement);
            }

            @Override
            public void failure(RetrofitError error) {
                nAnnouncement = "0";
                setDrawer(savedInstanceState, mActivity, true, getString(R.string.title_consignment_details), MainActivity.activity_cn, nAnnouncement);
            }
        });
    }

    public void setDrawer(Bundle savedInstanceState, Activity activity, boolean isHome, String title,
                          int position, String no_announcement) {

        mContext = activity.getApplicationContext();
        mActivity = activity;
        sessionUserData = new SessionUserData(mContext);
        // Handle Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(isHome);
        getSupportActionBar().setTitle(title);
        buildHeader(savedInstanceState, mActivity);

        result = new DrawerBuilder()
                .withActivity(mActivity)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withDrawerLayout(R.layout.crossfade_drawer)
                .withDrawerWidthDp(72)
                .withGenerateMiniDrawer(true)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
//                        0
                        new PrimaryDrawerItem().withName(R.string.title_home)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_home)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(0),
//                        1
                        new PrimaryDrawerItem().withName(R.string.title_consignment_list)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_list_alt)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(1)
                                .withEnabled(true),
//                        2
                        new PrimaryDrawerItem().withName(R.string.title_reports)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_file_text)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(2),
//                        3
                        new PrimaryDrawerItem().withName(R.string.title_profile)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_user)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(3),
//                        4
                        new PrimaryDrawerItem().withName(R.string.nav_item_announcement)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_bullhorn)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withBadge(no_announcement)
                                .withBadgeStyle(new BadgeStyle(Color.RED, Color.RED))
                                .withIdentifier(4),
//                        5
                        new PrimaryDrawerItem().withName(R.string.nav_item_check_update)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_globe)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(5),
//                        6
                        new PrimaryDrawerItem().withName(R.string.nav_item_attendence)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_check)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(6),
//                        7
                        new PrimaryDrawerItem().withName(R.string.nav_item_logout)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_sign_out)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(7)
                ) // add the items we want to use with our Drawer
                .withOnDrawerNavigationListener(new Drawer.OnDrawerNavigationListener() {
                    @Override
                    public boolean onNavigationClickListener(View clickedView) {
                        //this method is only called if the Arrow icon is shown. The hamburger is automatically managed by the MaterialDrawer
                        //if the back arrow is shown. close the activity
//                        mActivity.finish();
                        //return true if we have consumed the event
                        return true;
                    }
                })
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
//                        Toast.makeText(MapsActivity.this, "onDrawerOpened", Toast.LENGTH_SHORT).show();
                        KeyboardUtil.hideKeyboard(mActivity);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
//                        Toast.makeText(MapsActivity.this, "onDrawerClosed", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem instanceof Nameable) {
                            Toast.makeText(mContext, ((Nameable) drawerItem).getName()
                                    .getText(mContext), Toast.LENGTH_SHORT).show();
//                            0
                            if (drawerItem.getIdentifier() == 0) {
                                startActivity(new Intent(mContext, MainActivity.class));
                            }
//                            1
                            else if (drawerItem.getIdentifier() == 1) {
                                startActivity(new Intent(mContext, ActivityConsignmentList.class));
                            }
//                            2
                            else if (drawerItem.getIdentifier() == 2) {
                                startActivity(new Intent(mContext, ActivityReports.class));
                            }
//                            3
                            else if (drawerItem.getIdentifier() == 3) {
                                startActivity(new Intent(mContext, ActivityProfile.class));
                            }
//                            4
                            else if (drawerItem.getIdentifier() == 4) {
                                startActivity(new Intent(mContext, AnnouncementActivity.class));
                            }
//                            5
                            else if (drawerItem.getIdentifier() == 5) {
                                ActivityConsignmentDetails.GetVersionCode getVersionCode = new ActivityConsignmentDetails.GetVersionCode();
                                showProgressDialog(true, " Version Checking", "Please wait...");
                                getVersionCode.execute();
                            }
//                            6
                            else if (drawerItem.getIdentifier() == 6) {
                                startActivity(new Intent(mContext, AttendenceActivity.class));
                            }
//                            7
                            else if (drawerItem.getIdentifier() == 7) {
                                sessionUserData.endSession();
                                LoginActivity.deleteCache(mContext);
                                sessionUserData.checkLogin();
                                finish();
                            }
//                            10
                            else if (drawerItem.getIdentifier() == 10) {
                                showToast("Settings", Toast.LENGTH_SHORT, 0);
//                                startActivity(new Intent(context, SettingsActivity.class));
                            }
                        }
                        return false;
                    }
                })
                .addStickyDrawerItems(
//                        10
                        new PrimaryDrawerItem().withName(R.string.drawer_item_settings)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_cog)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(10)
                )
                .withSelectedItem(position)
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .build();

        crossfadeDrawerLayout = (CrossfadeDrawerLayout) result.getDrawerLayout();
        crossfadeDrawerLayout.setMaxWidthPx(DrawerUIUtils.getOptimalDrawerWidth(this));
        miniResult = result.getMiniDrawer();
        View view = miniResult.build(this);
        view.setBackgroundColor(
                UIUtils.getThemeColorFromAttrOrRes(
                        this,
                        com.mikepenz.materialdrawer.R.attr.material_drawer_background,
                        com.mikepenz.materialdrawer.R.color.material_drawer_background));
        crossfadeDrawerLayout.getSmallView().addView(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        miniResult.withCrossFader(new ICrossfader() {
            @Override
            public void crossfade() {
                boolean isFaded = isCrossfaded();
                crossfadeDrawerLayout.crossfade(400);

                //only close the drawer if we were already faded and want to close it now
                if (isFaded) {
                    result.getDrawerLayout().closeDrawer(GravityCompat.START);
                }
            }

            @Override
            public boolean isCrossfaded() {
                return crossfadeDrawerLayout.isCrossfaded();
            }
        });

    }

    private void buildHeader(Bundle savedInstanceState, Activity activity) {
        final ImagePopup imagePopup;imagePopup = new ImagePopup(this);
        imagePopup.setWindowWidth(800);
        imagePopup.setWindowHeight(800);
        imagePopup.setHideCloseIcon(true);
        imagePopup.setImageOnClickClose(true);
        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(activity)
                .withHeaderBackground(R.drawable.bg_nav_main)
                .addProfiles(profile)
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        String uName = profile.getName().getText();
                        if (user_name.equals(uName))
                            startActivity(new Intent(mContext, ActivityProfile.class));

                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        String uName = profile.getName().getText();
                        if (user_name.equals(uName)){
                            if (IMG_DRAWABLE != null)
                                imagePopup.initiatePopup(IMG_DRAWABLE);
//                            Bitmap bitmap = showProfileImageChangeDialog();
//
//                            if (bitmap != null){
//                                Log.e(TAG, bitmap.toString());
//                                mProfile = new ProfileDrawerItem().withName(user_name).withEmail(user_type).withIcon(bitmap);
//                                headerResult.updateProfile(mProfile);
//                            }
                        }
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();
    }

    public void setProfile(String name, String type, String pro_pic_url) {
        Log.e(TAG, pro_pic_url);
        if(pro_pic_url != null && !pro_pic_url.isEmpty() && !pro_pic_url.equals(""))
            profile = new ProfileDrawerItem().withName(name).withEmail(type).withIcon(pro_pic_url);

        else profile = new ProfileDrawerItem().withName(name).withEmail(type)
                .withIcon(getResources().getDrawable(R.drawable.ic_default_propic));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //add the values which need to be saved from the drawer to the bundle
        outState = result.saveInstanceState(outState);
        //add the values which need to be saved from the accountHeader to the bundle
        outState = headerResult.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    public class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + MainActivity.packageName + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (!MainActivity.currentVersion.equals(onlineVersion)) {
                    //show dialog
                    hasNew = true;
                    Log.e("Has New ", " "+hasNew);
                    startActivity(new Intent(mContext, ActivityCheckVersion.class));
                }
            }
            Log.e("update", "Current version " + MainActivity.currentVersion + "playstore version " + onlineVersion+" hasNext : "+hasNew);
            hideProgressDialog();
        }
    }

}
