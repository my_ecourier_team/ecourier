/*
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */

package com.ps.ecourier.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.mikepenz.crossfadedrawerlayout.view.CrossfadeDrawerLayout;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.MiniDrawer;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.interfaces.ICrossfader;
import com.mikepenz.materialdrawer.model.MiniDrawerItem;
import com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;
import com.mikepenz.materialdrawer.util.KeyboardUtil;
import com.mikepenz.materialize.util.UIUtils;
import com.ps.ecourier.R;
import com.ps.ecourier.activities.firstLayer.ActivityCheckVersion;
import com.ps.ecourier.activities.firstLayer.ActivityConsignmentList;
import com.ps.ecourier.activities.firstLayer.ActivityProfile;
import com.ps.ecourier.activities.firstLayer.ActivityReports;
import com.ps.ecourier.activities.firstLayer.AnnouncementActivity;
import com.ps.ecourier.activities.firstLayer.AttendenceActivity;
import com.ps.ecourier.activities.secondLayer.ActivityConsignmentDetails;
import com.ps.ecourier.base.BaseActivity;
import com.ps.ecourier.pojo.AnnouncementList;
import com.ps.ecourier.pojo.ConsignmentList;
import com.ps.ecourier.pojo.ConsignmentListDatum;
import com.ps.ecourier.services.LocationService;
import com.ps.ecourier.session.SessionUserData;
import com.ps.ecourier.utils.FormValidator;
import com.ps.ecourier.views.FragmentDialogQRScanner;
import com.ps.ecourier.webservices.ApiParams;
import com.ps.ecourier.webservices.interfaces.AnnouncementListInterface;
import com.ps.ecourier.webservices.interfaces.ConsignmentListInterface;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends BaseActivity implements View.OnClickListener{

    private AccountHeader headerResult = null;
    private Drawer result = null;
    private CrossfadeDrawerLayout crossfadeDrawerLayout = null;
    private IProfile mProfile;
    private MiniDrawer miniResult = null;
    private Context mContext;
    private Activity mActivity;
    public static HashMap<String, String> user = new HashMap<String, String>();
    private SessionUserData sessionUserData;
    private boolean hasNew = false;
    private Context context;
    private LocationService mLocationService;
    private Intent locationService;
    private EditText mSearchValue;
    private Button mSearch;
    private Button mQRScan;
    private TextView mTotalPicked, mTotalDelivered, mTotalReturned, mTotalProcessing, mTotalPrice;
    private Spinner searchValueSpinner;
    private ArrayAdapter<String> adapterSpinner;
    private String searchValues[];
    private String searchTypeValue, nAnnouncement;
    private final String[] finalSearchValues = new String[3];

    private List<ConsignmentListDatum> consignmentListDatumList;
    private ArrayList<String> searchValuesArray;
    private String TAG = "MainActivity";


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    showErrorToast("Cannot get Fine Location ", Toast.LENGTH_SHORT);
                }
            }
            case MY_PERMISSIONS_REQUEST_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    showErrorToast("Cannot get Coarse Location", Toast.LENGTH_SHORT);
                }
            }
            case CAMERA_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }

                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                    showErrorToast("You must provide access to use this app!", Toast.LENGTH_SHORT);
                }
                return;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showPermissionDialog();
        context = MainActivity.this;
        sessionUserData = new SessionUserData(this);

        user = sessionUserData.getSessionDetails();
        user_name = user.get(SessionUserData.KEY_USER_NAME);
        user_type = user.get(SessionUserData.KEY_USER_TYPE);
        if (user_type.contains("1")){
            user_type_name = "Admin";
        }
        else if (user_type.contains("2")){
            user_type_name = "Agent";
        }
        else user_type_name = "unknown";
        user_pro_pic = user.get(SessionUserData.KEY_USER_PROFILE_PICTURE);
        if(user_pro_pic.contains("test."))
            user_pro_pic = user_pro_pic.replace("test.", "");
        user_pro_pic = "http://"+user_pro_pic;
        Log.e(TAG, user_name + user_type_name + user_pro_pic);
        new drawable_from_url().execute(user_pro_pic);
        initialize();
        mContext = MainActivity.this;
        mActivity = MainActivity.this;
        Log.e(TAG, user_type+ " "+ user_type_name);
        setProfile(user_name, user_type_name, user_pro_pic);
        getAnnouncementNumber(savedInstanceState);
        locationService = new Intent(context, LocationService.class);
        try {
            packageName = getApplicationContext().getPackageName();
            currentVersion = getPackageManager().getPackageInfo(packageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getAnnouncementNumber(final Bundle savedInstanceState) {
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(ApiParams.TAG_BASE_URL).build();
        //creating a service for adapter with our ApiCallback class
        AnnouncementListInterface myApiCallback = restAdapter.create(AnnouncementListInterface.class);

        // get user data from session

        String user_type = user.get(SessionUserData.KEY_USER_TYPE);
        String id = user.get(SessionUserData.KEY_USER_ID);
        String group = user.get(SessionUserData.KEY_USER_GROUP);
        String authentication_key = user.get(SessionUserData.KEY_USER_AUTH_KEY);
        Log.e(TAG, user_type+" "+id+" "+group+" "+authentication_key);

        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(ApiParams.PARAM_AGENT_ID, id);
        map.put(ApiParams.PARAM_GROUP, group);
        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, authentication_key);
        map.put(ApiParams.PARAM_ANNOUNCEMENT_ID, "1");
        myApiCallback.getData(ApiParams.TAG_ANNOUNCEMENT_KEY, map, new Callback<AnnouncementList>() {
            @Override
            public void success(AnnouncementList announcementList, Response response) {
                nAnnouncement = announcementList.getNo_of_items();
                setDrawer(savedInstanceState, mActivity, false, getString(R.string.nav_item_home), activity_home, nAnnouncement);
            }

            @Override
            public void failure(RetrofitError error) {
                nAnnouncement = "0";
                setDrawer(savedInstanceState, mActivity, false, getString(R.string.nav_item_home), activity_home, nAnnouncement);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

            AlertDialog.Builder alert_box = new AlertDialog.Builder(this);
            alert_box.setTitle(getResources().getString(R.string.exit_title));
            alert_box.setMessage(getResources().getString(R.string.exit_confirmation));

            alert_box.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            finish();
                        }
                    });

            alert_box.setNeutralButton("No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    });

            alert_box.show();

            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private void showPermissionDialog() {
        if (!LocationService.checkPermission(this)) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION);

            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_COARSE_LOCATION);

        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        mLocationService = new LocationService(context);
        return false;
    }

    private void initialize() {
        finalSearchValues[0] = ApiParams.TAG_CONSIGNMENT_NO;
        finalSearchValues[1] = ApiParams.TAG_RECEIVER_NO;
        finalSearchValues[2] = ApiParams.TAG_PRODUCT_ID;
        mTotalPicked = (TextView) findViewById(R.id.textTotalPicked);
        mTotalDelivered = (TextView) findViewById(R.id.textTotalDelivered);
        mTotalReturned = (TextView) findViewById(R.id.textTotalReturned);
        mTotalProcessing = (TextView) findViewById(R.id.textTotalProcessing);
        mTotalPrice = (TextView) findViewById(R.id.textTotalPrice);
        mSearchValue = (EditText) findViewById(R.id.editSearchValue);
        mSearch = (Button) findViewById(R.id.btnSearch);
        mSearch.setOnClickListener(this);
        mQRScan = (Button) findViewById(R.id.btnScan);
        mQRScan.setOnClickListener(this);
        searchValuesArray = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.searchValuesArray)));
        searchValues = new String[searchValuesArray.size()];
        searchValues = searchValuesArray.toArray(searchValues);
        searchValueSpinner = (Spinner) findViewById(R.id.spinnerSearchValue);
        adapterSpinner = new ArrayAdapter<String>(context, R.layout.spinner_item, R.id.textSearchValue, searchValues);
        searchValueSpinner.setAdapter(adapterSpinner);
        searchValueSpinner.setSelection(0);
        searchValueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    searchTypeValue = finalSearchValues[position];
                    mSearchValue.setHint(searchTypeValue.toUpperCase());

                } else {
                    searchTypeValue = finalSearchValues[0];
                    mSearchValue.setHint(searchTypeValue.toUpperCase());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                searchTypeValue = finalSearchValues[0];
                mSearchValue.setHint(searchTypeValue.toUpperCase());
            }
        });
        // get user data from session
        String name = user.get(SessionUserData.KEY_USER_NAME);
        String total_picked = user.get(SessionUserData.KEY_USER_TOTAL_PICKED);
        String total_delivered = user.get(SessionUserData.KEY_USER_TOTAL_DELIVERED);
        String total_returned = user.get(SessionUserData.KEY_USER_TOTAL_RETURNED);
        String total_processing = user.get(SessionUserData.KEY_USER_TOTAL_PROCESSING);
        String total_price = user.get(SessionUserData.KEY_USER_TOTAL_DELIVERED_PRODUCT_PRICE);

        mTotalPicked.setText("Total Picked: " + total_picked);
        mTotalDelivered.setText("Total Delivered: " + total_delivered);
        mTotalReturned.setText("Total Returned: " + total_returned);
        mTotalProcessing.setText("Total Processing: " + total_processing);
        mTotalPrice.setText("Total Delivered Product Price: " + total_price);
    }

    private void getRequiredPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            showErrorToast("You must provide access to use this app!", Toast.LENGTH_SHORT);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSIONS_REQUEST);
        }
    }

    private String getSearchTypeValue(){
        return searchTypeValue;
    }

    @Override
    public void onClick(View v) {

        FormValidator fv = new FormValidator();
        String search_value = mSearchValue.getText().toString();

        switch (v.getId()) {
            case R.id.btnSearch:
                if (!fv.isValidField(search_value)) {
                    mSearchValue.setError(getResources().getString(R.string.empty_field));
                } else {
                    searchConsignment(search_value);
                }
                break;

            case R.id.btnScan:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    FragmentManager fm = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        fm = this.getFragmentManager();
                    }
                    @SuppressLint("ValidFragment")
                    FragmentDialogQRScanner fragmentDialogQRScanner = new FragmentDialogQRScanner() {

                        @Override
                        public void success(String value, String format_type) {
                            searchConsignment(value);
                        }

                        @Override
                        public void error() {

                        }
                    };
                    fragmentDialogQRScanner.setArgs("", "", "");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        fragmentDialogQRScanner.show(fm, "fragment_qr_scanner");
                    }
                } else {
                    getRequiredPermission();
                }
                break;
        }
    }

    private void searchConsignment(String search_value) {
        showProgressDialog(false, "", getResources().getString(R.string.loading));
//        getSearchTypeValue();

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(ApiParams.TAG_BASE_URL).build();
        ConsignmentListInterface myApiCallback = restAdapter.create(ConsignmentListInterface.class);

        // get user data from session
        String user_type = user.get(SessionUserData.KEY_USER_TYPE);
        String id = user.get(SessionUserData.KEY_USER_ID);
        String group = user.get(SessionUserData.KEY_USER_GROUP);
        String authentication_key = user.get(SessionUserData.KEY_USER_AUTH_KEY);

        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();

        if (user_type.equals("1")) {
            map.put(ApiParams.PARAM_ADMIN_ID, "" + id);
        } else if (user_type.equals("2")) {
            map.put(ApiParams.PARAM_AGENT_ID, "" + id);
        }
        map.put(ApiParams.PARAM_GROUP, group);
        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, "" + authentication_key);
        map.put(searchTypeValue, "" + search_value);

        myApiCallback.getData(ApiParams.TAG_CONSIGNMENT_SEARCH_KEY, map, new Callback<ConsignmentList>() {
            @Override
            public void success(ConsignmentList consignment_list, Response response) {
                hideProgressDialog();

                clearData();

                String status = consignment_list.getStatus();
                if (status.equals(ApiParams.TAG_SUCCESS)) {

                    consignmentListDatumList = consignment_list.getData();

                    // convert the list to array list and pass via intent
                    ArrayList<ConsignmentListDatum> ItemArray = ((ArrayList<ConsignmentListDatum>) consignmentListDatumList);
//                    ConsignmentListDatum datum = ItemArray.get(0);

                    Intent i = new Intent(context, ActivityConsignmentDetails.class);
                    i.putExtra("is_search", true);
                    i.putExtra("visible_state", "0");
                    i.putExtra("consignment_data_position", "0");
                    i.putExtra("consignment_data_array", ItemArray);
//                    i.putExtra("consignment_data", datum);
                    i.putExtra("where_from", "search");
                    startActivity(i);
                    showToast("" + consignment_list.getStatus() + "!", 0);
                }
                else
                    showErrorToast(getString(R.string.no_data_found), 0);

            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialog();
                showErrorToast("" + error.getMessage() + "!", 0);
            }
        });
    }

    private void clearData() {
        mSearchValue.setText("");
    }


    public void setDrawer(Bundle savedInstanceState, Activity activity, boolean isHome, String title,
                          int position, String no_announcement) {
        Log.e(TAG +" drawer", no_announcement);
        mContext = activity.getApplicationContext();
        mActivity = activity;
        sessionUserData = new SessionUserData(mContext);

        // Handle Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(isHome);
        getSupportActionBar().setTitle(title);
        buildHeader(savedInstanceState, mActivity);

        result = new DrawerBuilder()
                .withActivity(mActivity)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withDrawerLayout(R.layout.crossfade_drawer)
                .withDrawerWidthDp(72)
                .withGenerateMiniDrawer(true)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.title_home)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_home)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(0)
                                .withEnabled(false),
                        new PrimaryDrawerItem().withName(R.string.title_consignment_list)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_list_alt)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.title_reports)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_file_text)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(2),
                        new PrimaryDrawerItem().withName(R.string.title_profile)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_user)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.nav_item_announcement)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_bullhorn)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withBadge(no_announcement)
                                .withBadgeStyle(new BadgeStyle(Color.RED, Color.RED))
                                .withIdentifier(4),
                        new PrimaryDrawerItem().withName(R.string.nav_item_check_update)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_globe)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(5),
                        new PrimaryDrawerItem().withName(R.string.nav_item_attendence)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_check)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(6),
                        new PrimaryDrawerItem().withName(R.string.nav_item_logout)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_sign_out)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(7)
                ) // add the items we want to use with our Drawer
                .withOnDrawerNavigationListener(new Drawer.OnDrawerNavigationListener() {
                    @Override
                    public boolean onNavigationClickListener(View clickedView) {
                        //this method is only called if the Arrow icon is shown. The hamburger is automatically managed by the MaterialDrawer
                        //if the back arrow is shown. close the activity
//                        mActivity.finish();
                        //return true if we have consumed the event
                        return true;
                    }
                })
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
//                        Toast.makeText(MapsActivity.this, "onDrawerOpened", Toast.LENGTH_SHORT).show();
                        KeyboardUtil.hideKeyboard(mActivity);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
//                        Toast.makeText(MapsActivity.this, "onDrawerClosed", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem instanceof Nameable) {
                            Toast.makeText(mContext, ((Nameable) drawerItem).getName()
                                    .getText(mContext), Toast.LENGTH_SHORT).show();

                            if (drawerItem.getIdentifier() == 0) {
                                startActivity(new Intent(mContext, MainActivity.class));
                            }
                            else if (drawerItem.getIdentifier() == 1) {
                                startActivity(new Intent(mContext, ActivityConsignmentList.class));
                            }
                            else if (drawerItem.getIdentifier() == 2) {
                                startActivity(new Intent(mContext, ActivityReports.class));
                            }
                            else if (drawerItem.getIdentifier() == 3) {
                                startActivity(new Intent(mContext, ActivityProfile.class));
                            }
                            else if (drawerItem.getIdentifier() == 4) {
                                startActivity(new Intent(mContext, AnnouncementActivity.class));
                            }
                            else if (drawerItem.getIdentifier() == 5) {
                                MainActivity.GetVersionCode getVersionCode = new MainActivity.GetVersionCode();
                                showProgressDialog(true, " Version Checking", "Please wait...");
                                getVersionCode.execute();
                            }
                            else if (drawerItem.getIdentifier() == 6) {
                                startActivity(new Intent(mContext, AttendenceActivity.class));
                            }
                            else if (drawerItem.getIdentifier() == 7) {
                                sessionUserData.endSession();
                                LoginActivity.deleteCache(mContext);
                                sessionUserData.checkLogin();
                                finish();
                            }
                            else if (drawerItem.getIdentifier() == 10) {
                                showToast("Settings", Toast.LENGTH_SHORT, 0);
//                                startActivity(new Intent(context, SettingsActivity.class));
                            }
                        }
                        return false;
                    }
                })
                .addStickyDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_settings)
                                .withIcon(new IconicsDrawable(this, FontAwesome.Icon.faw_cog)
                                        .actionBar()
                                        .paddingDp(5)
                                        .colorRes(R.color.material_drawer_dark_primary_text))
                                .withSelectedIconColor(Color.RED)
                                .withIconTintingEnabled(true)
                                .withIdentifier(10)
                )
                .withSelectedItem(position)
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .build();

        result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(!isHome);

        //get the CrossfadeDrawerLayout which will be used as alternative DrawerLayout for the Drawer
        //the CrossfadeDrawerLayout library can be found here: https://github.com/mikepenz/CrossfadeDrawerLayout
        crossfadeDrawerLayout = (CrossfadeDrawerLayout) result.getDrawerLayout();

        //define maxDrawerWidth
        crossfadeDrawerLayout.setMaxWidthPx(DrawerUIUtils.getOptimalDrawerWidth(this));
        //add second view (which is the miniDrawer)
        miniResult = result.getMiniDrawer();
        //build the view for the MiniDrawer
        View view = miniResult.build(this);
        //set the background of the MiniDrawer as this would be transparent
        view.setBackgroundColor(UIUtils.getThemeColorFromAttrOrRes(this, com.mikepenz.materialdrawer.R.attr.material_drawer_background, com.mikepenz.materialdrawer.R.color.material_drawer_background));
        //we do not have the MiniDrawer view during CrossfadeDrawerLayout creation so we will add it here
        crossfadeDrawerLayout.getSmallView().addView(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        //define the crossfader to be used with the miniDrawer. This is required to be able to automatically toggle open / close
        miniResult.withCrossFader(new ICrossfader() {
            @Override
            public void crossfade() {
                boolean isFaded = isCrossfaded();
                crossfadeDrawerLayout.crossfade(400);

                //only close the drawer if we were already faded and want to close it now
                if (isFaded) {
                    result.getDrawerLayout().closeDrawer(GravityCompat.START);
                }
            }

            @Override
            public boolean isCrossfaded() {
                return crossfadeDrawerLayout.isCrossfaded();
            }
        });


        /**
         * NOTE THIS IS A HIGHLY CUSTOM ANIMATION. USE CAREFULLY.
         * this animate the height of the mProfile to the height of the AccountHeader and
         * animates the height of the drawerItems to the normal drawerItems so the difference between Mini and normal Drawer is eliminated
         **/

        final double headerHeight = DrawerUIUtils.getOptimalDrawerWidth(this) * 9d / 16d;
        final double originalProfileHeight = UIUtils.convertDpToPixel(72, this);
        final double headerDifference = headerHeight - originalProfileHeight;
        final double originalItemHeight = UIUtils.convertDpToPixel(64, this);
        final double normalItemHeight = UIUtils.convertDpToPixel(48, this);
        final double itemDifference = originalItemHeight - normalItemHeight;
        crossfadeDrawerLayout.withCrossfadeListener(new CrossfadeDrawerLayout.CrossfadeListener() {
            @Override
            public void onCrossfade(View containerView, float currentSlidePercentage, int slideOffset) {
                for (int i = 0; i < miniResult.getAdapter().getItemCount(); i++) {
                    IDrawerItem drawerItem = miniResult.getAdapter().getItem(i);
                    if (drawerItem instanceof MiniProfileDrawerItem) {
                        MiniProfileDrawerItem mpdi = (MiniProfileDrawerItem) drawerItem;
                        mpdi.withCustomHeightPx((int) (originalProfileHeight + (headerDifference * currentSlidePercentage / 100)));
                    } else if (drawerItem instanceof MiniDrawerItem) {
                        MiniDrawerItem mdi = (MiniDrawerItem) drawerItem;
                        mdi.withCustomHeightPx((int) (originalItemHeight - (itemDifference * currentSlidePercentage / 100)));
                    }
                }
                miniResult.getAdapter().notifyDataSetChanged();
            }
        });

    }

    private void buildHeader(Bundle savedInstanceState, Activity activity) {
//        popup init
        final ImagePopup imagePopup;imagePopup = new ImagePopup(this);
        imagePopup.setWindowWidth(800);
        imagePopup.setWindowHeight(800);
        imagePopup.setHideCloseIcon(true);
        imagePopup.setImageOnClickClose(true);
        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(activity)
                .withHeaderBackground(R.drawable.bg_nav_main)
                .addProfiles(mProfile)
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        String uName = profile.getName().getText();
                        if (user_name.equals(uName))
                            startActivity(new Intent(mContext, ActivityProfile.class));

                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        String uName = profile.getName().getText();
                        if (user_name.equals(uName)){
                            if (IMG_DRAWABLE != null)
                                imagePopup.initiatePopup(IMG_DRAWABLE);
                        }
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();
    }

    public void setProfile(String name, String type, String pro_pic_url) {
        Log.e(TAG, pro_pic_url);
        if(pro_pic_url != null && !pro_pic_url.isEmpty() && !pro_pic_url.equals(""))
            mProfile = new ProfileDrawerItem().withName(name).withEmail(type).withIcon(pro_pic_url);

        else mProfile = new ProfileDrawerItem().withName(name).withEmail(type)
                .withIcon(getResources().getDrawable(R.drawable.ic_default_propic));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //add the values which need to be saved from the drawer to the bundle
        outState = result.saveInstanceState(outState);
        //add the values which need to be saved from the accountHeader to the bundle
        outState = headerResult.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    public class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + packageName + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (!currentVersion.equals(onlineVersion)) {
                    //show dialog
                    hasNew = true;
                    Log.e("Has New ", " "+hasNew);
                    startActivity(new Intent(mContext, ActivityCheckVersion.class));
                }
                else
                    Toast.makeText(mContext, R.string.appIsUpdated, Toast.LENGTH_SHORT).show();
            }
            Log.e("update", "Current version " + currentVersion + "playstore version " + onlineVersion+" hasNext : "+hasNew);
            hideProgressDialog();
        }
    }

}
