/*
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */

package com.ps.ecourier.activities.firstLayer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ps.ecourier.R;
import com.ps.ecourier.activities.secondLayer.ActivityConsignmentDetails;
import com.ps.ecourier.base.BaseActivity;
import com.ps.ecourier.pojo.ConsignmentList;
import com.ps.ecourier.pojo.ConsignmentListDatum;
import com.ps.ecourier.session.SessionUserData;
import com.ps.ecourier.utils.FormValidator;
import com.ps.ecourier.views.FragmentDialogQRScanner;
import com.ps.ecourier.webservices.ApiParams;
import com.ps.ecourier.webservices.interfaces.ConsignmentListInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.ps.ecourier.activities.MainActivity.user;

public class ActivityHome extends BaseActivity implements View.OnClickListener {

    private int CAMERA_PERMISSIONS_REQUEST = 8887;

    private Context context;
    private EditText mSearchValue;
    private Button mSearch;
    private Button mQRScan;
    private TextView mTotalPicked, mTotalDelivered, mTotalReturned, mTotalProcessing, mTotalPrice;
    private Spinner searchValueSpinner;
    private ArrayAdapter<String> adapterSpinner;
    private String searchValues[];
    private String searchTypeValue;
    private final String[] finalSearchValues = new String[3];

    private List<ConsignmentListDatum> consignmentListDatumList;
    private ArrayList<String> searchValuesArray;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        context = ActivityHome.this;

        initialize();
    }

    private void initialize() {
        finalSearchValues[0] = ApiParams.TAG_CONSIGNMENT_NO;
        finalSearchValues[1] = ApiParams.TAG_RECEIVER_NO;
        finalSearchValues[2] = ApiParams.TAG_PRODUCT_ID;
        mTotalPicked = (TextView) findViewById(R.id.textTotalPicked);
        mTotalDelivered = (TextView) findViewById(R.id.textTotalDelivered);
        mTotalReturned = (TextView) findViewById(R.id.textTotalReturned);
        mTotalProcessing = (TextView) findViewById(R.id.textTotalProcessing);
        mTotalPrice = (TextView) findViewById(R.id.textTotalPrice);
        mSearchValue = (EditText) findViewById(R.id.editSearchValue);
        mSearch = (Button) findViewById(R.id.btnSearch);
        mSearch.setOnClickListener(this);
        mQRScan = (Button) findViewById(R.id.btnScan);
        mQRScan.setOnClickListener(this);
        searchValuesArray = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.searchValuesArray)));
        searchValues = new String[searchValuesArray.size()];
        searchValues = searchValuesArray.toArray(searchValues);
        searchValueSpinner = (Spinner) findViewById(R.id.spinnerSearchValue);
        adapterSpinner = new ArrayAdapter<String>(context, R.layout.spinner_item, R.id.textSearchValue, searchValues);
        searchValueSpinner.setAdapter(adapterSpinner);
        searchValueSpinner.setSelection(0);
        searchValueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    searchTypeValue = finalSearchValues[position];
                    mSearchValue.setHint(searchTypeValue.toUpperCase());

                } else {
                    searchTypeValue = finalSearchValues[0];
                    mSearchValue.setHint(searchTypeValue.toUpperCase());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                searchTypeValue = finalSearchValues[0];
                mSearchValue.setHint(searchTypeValue.toUpperCase());
            }
        });
        // get user data from session
        String name = user.get(SessionUserData.KEY_USER_NAME);
        String total_picked = user.get(SessionUserData.KEY_USER_TOTAL_PICKED);
        String total_delivered = user.get(SessionUserData.KEY_USER_TOTAL_DELIVERED);
        String total_returned = user.get(SessionUserData.KEY_USER_TOTAL_RETURNED);
        String total_processing = user.get(SessionUserData.KEY_USER_TOTAL_PROCESSING);
        String total_price = user.get(SessionUserData.KEY_USER_TOTAL_DELIVERED_PRODUCT_PRICE);

        mTotalPicked.setText("Total Picked: " + total_picked);
        mTotalDelivered.setText("Total Delivered: " + total_delivered);
        mTotalReturned.setText("Total Returned: " + total_returned);
        mTotalProcessing.setText("Total Processing: " + total_processing);
        mTotalPrice.setText("Total Delivered Product Price: " + total_price);
    }

    private void getRequiredPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            showErrorToast("You must provide access to use this app!", Toast.LENGTH_SHORT);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSIONS_REQUEST);
        }
    }

    private String getSearchTypeValue(){
        return searchTypeValue;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 8887: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }

                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                    showErrorToast("You must provide access to use this app!", Toast.LENGTH_SHORT);
                }
                return;
            }
        }
    }

    @Override
    public void onClick(View v) {

        FormValidator fv = new FormValidator();
        String search_value = mSearchValue.getText().toString();

        switch (v.getId()) {
            case R.id.btnSearch:
                if (!fv.isValidField(search_value)) {
                    mSearchValue.setError(getResources().getString(R.string.empty_field));
                } else {
                    searchConsignment(search_value);
                }
                break;

            case R.id.btnScan:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    FragmentManager fm = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        fm = this.getFragmentManager();
                    }
                    @SuppressLint("ValidFragment")
                    FragmentDialogQRScanner fragmentDialogQRScanner = new FragmentDialogQRScanner() {

                        @Override
                        public void success(String value, String format_type) {
                            searchConsignment(value);
                        }

                        @Override
                        public void error() {

                        }
                    };
                    fragmentDialogQRScanner.setArgs("", "", "");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        fragmentDialogQRScanner.show(fm, "fragment_qr_scanner");
                    }
                } else {
                    getRequiredPermission();
                }
                break;
        }
    }

    private void searchConsignment(String search_value) {
        showProgressDialog(false, "", getResources().getString(R.string.loading));
//        getSearchTypeValue();

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(ApiParams.TAG_BASE_URL).build();
        ConsignmentListInterface myApiCallback = restAdapter.create(ConsignmentListInterface.class);

        // get user data from session
        String user_type = user.get(SessionUserData.KEY_USER_TYPE);
        String id = user.get(SessionUserData.KEY_USER_ID);
        String group = user.get(SessionUserData.KEY_USER_GROUP);
        String authentication_key = user.get(SessionUserData.KEY_USER_AUTH_KEY);

        //Lets pass the desired parameters
        HashMap<String, String> map = new HashMap<String, String>();

        if (user_type.equals("1")) {
            map.put(ApiParams.PARAM_ADMIN_ID, "" + id);
        } else if (user_type.equals("2")) {
            map.put(ApiParams.PARAM_AGENT_ID, "" + id);
        }
        map.put(ApiParams.PARAM_GROUP, group);
        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, "" + authentication_key);
        map.put(searchTypeValue, "" + search_value);

        myApiCallback.getData(ApiParams.TAG_CONSIGNMENT_SEARCH_KEY, map, new Callback<ConsignmentList>() {
            @Override
            public void success(ConsignmentList consignment_list, Response response) {
                hideProgressDialog();

                clearData();

                String status = consignment_list.getStatus();
                if (status.equals(ApiParams.TAG_SUCCESS)) {

                    consignmentListDatumList = consignment_list.getData();

                    // convert the list to array list and pass via intent
                    ArrayList<ConsignmentListDatum> ItemArray = ((ArrayList<ConsignmentListDatum>) consignmentListDatumList);
//                    ConsignmentListDatum datum = ItemArray.get(0);

                    Intent i = new Intent(context, ActivityConsignmentDetails.class);
                    i.putExtra("is_search", true);
                    i.putExtra("visible_state", "0");
                    i.putExtra("consignment_data_position", "0");
                    i.putExtra("consignment_data_array", ItemArray);
//                    i.putExtra("consignment_data", datum);
                    i.putExtra("where_from", "search");
                    startActivity(i);
                    showToast("" + consignment_list.getStatus() + "!", 0);
                }
                else
                    showErrorToast(getString(R.string.no_data_found), 0);

            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialog();
                showErrorToast("" + error.getMessage() + "!", 0);
            }
        });
    }

    private void clearData() {
        mSearchValue.setText("");
    }
}
