package com.ps.ecourier.base;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;

import com.ps.ecourier.views.CustomToast;

public class BaseFragment extends Fragment {

    private CustomToast customToast;
    private ProgressDialog progressDialog;

    public void showToast(String message, int duration) {
        customToast = new CustomToast();
        customToast.showDeafultToast(getActivity(), message, duration);
    }

    public void showErrorToast(String message, int duration) {
        customToast = new CustomToast();
        customToast.showErrorToast(getActivity(), message, duration);
    }

    public void hideDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }

    public void showDialog(boolean show_title, String title, String message) {
        progressDialog = new ProgressDialog(getActivity());
        if (show_title) {
            progressDialog.setTitle(title);
        }
        progressDialog.setMessage(message);
        progressDialog.show();

    }
}
