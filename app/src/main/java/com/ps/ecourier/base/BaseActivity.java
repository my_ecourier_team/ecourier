package com.ps.ecourier.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ps.ecourier.R;
import com.ps.ecourier.pojo.ProfileList;
import com.ps.ecourier.pojo.ProfileListDatum;
import com.ps.ecourier.session.SessionUserData;
import com.ps.ecourier.views.CustomToast;
import com.ps.ecourier.webservices.ApiParams;
import com.ps.ecourier.webservices.interfaces.ProfileListInterface;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.ps.ecourier.activities.MainActivity.user;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */
public class BaseActivity extends AppCompatActivity {

//    static variable
    public static final String ALBUM_NAME = "eCourier";
    public static String currentVersion, packageName, user_name, user_type, user_type_name, user_pro_pic, user_announcement_no;
    public static final int activity_none = -1, activity_home = 0, activity_cn = 1, activity_report = 2, activity_profile = 3,
            activity_announcement = 4, activity_update = 5, activity_attendence = 6, activity_settings = 10;
    public static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 101;
    public static final int MY_PERMISSIONS_REQUEST_COARSE_LOCATION = 102;
    public static final long MIN_DISTANCE_FOR_UPDATE = 103;
    public static final long MIN_TIME_FOR_UPDATE = 1000 * 60 * 5;
    public static final int CAMERA_PERMISSIONS_REQUEST = 301;
    public static final int SELECT_PICTURE = 104,
            TAKE_PHOTO = 105,
            REQUEST_EXTERNAL_STORAGE = 107,
            REQUEST_READ_EXTERNAL_STORAGE = 108;
    public static Drawable IMG_DRAWABLE = null;
    public static Bitmap mBitmap = null;

    private CustomToast customToast;
    private ProgressDialog progressDialog;
    private AlertDialog dialog;
    private View dialogViewProfile;
    private Button btnTakePhoto, btnOpenGallery;
    private File mPhoto;
    private String path = "";
    private String TAG = "Base Activity";
    private ArrayList<String> image_list=new ArrayList<String>();
    private ArrayList<Drawable> image_drawable=new ArrayList<Drawable>();

    public void showToast(String message, int duration, int gravity) {
        customToast = new CustomToast();
        customToast.showDeafultToast(this, message, duration, gravity);
    }

    public void showErrorToast(String message, int duration, int gravity) {
        customToast = new CustomToast();
        customToast.showErrorToast(this, message, duration, gravity);
    }

    public void showToast(String message, int duration) {
        customToast = new CustomToast();
        customToast.showDeafultToast(this, message, duration);
    }

    public void showSuccessToast(String message, int duration) {
        customToast = new CustomToast();
        customToast.showToast(this, Color.GREEN, Color.WHITE, message, duration);
    }
    public void showSuccessToast(Activity activity, String message, int duration) {
        customToast = new CustomToast();
        customToast.showToast(activity, Color.GREEN, Color.WHITE, message, duration);
    }

    public void showErrorToast(String message, int duration) {
        customToast = new CustomToast();
        customToast.showErrorToast(this, message, duration);
    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }

    public void showProgressDialog(boolean show_title, String title, String message) {
        progressDialog = new ProgressDialog(this);
        if (show_title) {
            progressDialog.setTitle(title);
        }
        progressDialog.setMessage(message);
        progressDialog.show();

    }

    public Bitmap showProfileImageChangeDialog (){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        dialogViewProfile = inflater.inflate(R.layout.dialog_profile_pic, null);
        dialogBuilder.setView(dialogViewProfile);
        dialog = dialogBuilder.create();
        dialog.setCancelable(true);
        dialog.show();

        mBitmap = initDialog(dialogViewProfile);

//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms
//
//            }
//        }, 100);
        return mBitmap;
    }

    private Bitmap initDialog(final View dialogView) {
        btnTakePhoto = (Button) dialogView.findViewById(R.id.btnTakePhoto);
        btnOpenGallery = (Button) dialogView.findViewById(R.id.btnOpenGallery);
//        take photo
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                takePhoto();
            }
        });
//      open gallery
        btnOpenGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });

        return mBitmap;
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    private void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File folder = new File(Environment.getExternalStorageDirectory() + "/"+ALBUM_NAME);

        if(!folder.exists())
        {
            folder.mkdir();
        }

        path = String.format(Environment.getExternalStorageDirectory() +"/"+ALBUM_NAME+"_%d.jpg", System.currentTimeMillis());
        mPhoto = new File(path);
        Uri uri = FileProvider.getUriForFile(
                this,
                getApplicationContext().getPackageName()+".provider",
                mPhoto);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, TAKE_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
//            open gallery
            if (requestCode == SELECT_PICTURE) {
                // Get the url from data
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    // Get the path from the Uri
                    String path = getPathFromURI(selectedImageUri);
                    mPhoto = new File(path);
                    // Set the image in ImageView

                    try {
                        mBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.e(TAG, "Image file name : " + mPhoto.getName());
                    Log.e(TAG, Uri.fromFile(mPhoto).toString());
//                    sendFile(mPhoto);
                }
            }
//            take photo camera
            if(requestCode == TAKE_PHOTO) {
                image_list.add(path);
                new GetImages().execute();
            }
        }
    }

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = "";
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getApplicationContext().getContentResolver().query(contentUri, proj, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();
        int column_index = cursor.getColumnIndex(proj[0]);
        res = cursor.getString(column_index);
        cursor.close();
        Log.e(TAG, "Gallery File Path=====>>>"+contentUri);
        return res;
    }

    private class GetImages extends AsyncTask<Void, Void, Bitmap>
    {
        @Override
        protected Bitmap doInBackground(Void... params)
        {
            Bitmap bitmap;
            image_drawable.clear();
            for(int i=0; i<image_list.size(); i++) {
                bitmap = rotateBitmapOrientation(path);
                return bitmap;
            }
            return null;
        }

        protected void onPostExecute(Bitmap bitmap) {
            mBitmap = bitmap;
            Log.e(TAG, "Image file name : " + mPhoto.getName());
            Log.e(TAG, Uri.fromFile(mPhoto).toString());
//            sendFile(mPhoto);
        }
    }

    public Bitmap rotateBitmapOrientation(String photoFilePath) {

        // Create and configure BitmapFactory
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoFilePath, bounds);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory.decodeFile(photoFilePath, opts);
        // Read EXIF Data
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(photoFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;
        // Rotate Bitmap
        Matrix matrix = new Matrix();
        matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
        // Return result
        return rotatedBitmap;
    }

    public class drawable_from_url extends AsyncTask<String, Void, HttpURLConnection>
    {
        @Override
        protected HttpURLConnection doInBackground(String... urls) {
            Bitmap x;
            String url = urls[0];
            HttpURLConnection connection = null;
            try {
                connection = (HttpURLConnection) new URL(url).openConnection();
                connection.connect();
                InputStream input = connection.getInputStream();
                x = BitmapFactory.decodeStream(input);
                IMG_DRAWABLE = new BitmapDrawable(getResources(), x);
                return connection;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return connection;
        }

        protected void onPostExecute(HttpURLConnection connection) {
            connection.disconnect();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_refresh:
                Toast.makeText(getApplicationContext(), "Refreshing...", Toast.LENGTH_SHORT).show();
                break;

        }
        return false;
    }

    public void getProfileData(final String tag) {
        showProgressDialog(false, "", "Refreshing...");
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(ApiParams.TAG_BASE_URL).build();
        ProfileListInterface myApiCallback = restAdapter.create(ProfileListInterface.class);

        HashMap<String, String> map = new HashMap<String, String>();

        for (String name: user.keySet()){
            String key =name.toString();
            String value = user.get(name).toString();
            Log.e(tag+" 1",key + " " + value);
        }

//                Log.e(TAG, user_type);

        if (user.get(SessionUserData.KEY_USER_TYPE).equals("1")) {
            map.put(ApiParams.PARAM_ADMIN_ID, "" + user.get(SessionUserData.KEY_USER_ID));
        } else if (user.get(SessionUserData.KEY_USER_TYPE).equals("2")) {
            map.put(ApiParams.PARAM_AGENT_ID, "" + user.get(SessionUserData.KEY_USER_ID));
        }
        map.put(ApiParams.PARAM_GROUP, "" + user.get(SessionUserData.KEY_USER_GROUP));
        map.put(ApiParams.PARAM_AUTHENTICATION_KEY, "" + user.get(SessionUserData.KEY_USER_AUTH_KEY));

        for (String name: map.keySet()){
            String key =name.toString();
            String value = map.get(name).toString();
            Log.e(tag+" 2",key + " " + value);
        }

        myApiCallback.getData(ApiParams.TAG_PROFILE_KEY, map, new Callback<ProfileList>() {
            @Override
            public void success(ProfileList profileList, Response response) {
                //we get json object from server to our POJO or model class

                hideProgressDialog();

                boolean status = profileList.getStatus();
                if (status) {
                    ProfileListDatum profileListDatum = profileList.getData();
                    Log.e(tag, profileListDatum.toString());
                    String url = "";
                    url = profileList.getData().getProfilePic();
                    if (checkText(url)){
                        if(url.contains("test."))
                            url = url.replace("test.", "");
                        user_pro_pic = "http://"+url;
                    }

                } else {
                    showErrorToast(getString(R.string.no_data_found), 0);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideProgressDialog();
                showErrorToast("" + error.getMessage() + "!", 0);
            }
        });

    }

    private boolean checkText(String s) {
        boolean isValid;
        if(s != null && !s.isEmpty())
            isValid = true;
        else isValid = false;

        return isValid;
    }
}
