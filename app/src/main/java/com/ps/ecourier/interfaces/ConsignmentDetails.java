package com.ps.ecourier.interfaces;

import com.ps.ecourier.pojo.ConsignmentListDatum;

import java.util.List;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */
public interface ConsignmentDetails {

    public void setConsignmentDetails(List<ConsignmentListDatum> consignmentListDatums, int position);
}
