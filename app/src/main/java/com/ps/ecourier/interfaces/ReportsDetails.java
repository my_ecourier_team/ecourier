package com.ps.ecourier.interfaces;

import com.ps.ecourier.pojo.ReportsListDatum;

import java.util.List;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */
public interface ReportsDetails {
    public void setReportsDetails(List<ReportsListDatum> reportsListDatums, int position);
}
