/*
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */

package com.ps.ecourier.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ps.ecourier.R;
import com.ps.ecourier.pojo.AnnouncementListDatum;

import java.util.List;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */
public class AnnouncementListAdapter extends BaseAdapter {
    private Activity activity;
    private Context context;
    private LayoutInflater inflater;
    private List<AnnouncementListDatum> announcementListDatumList;
    private String TAG = "AnnouncementListAdapter";
    private int lastPosition = -1;
    private TextView textTitle, textPublishDate;
    private String mContent = "";

    public AnnouncementListAdapter(Context context, List<AnnouncementListDatum> announcementListDatumList) {
        this.context = context;
        this.announcementListDatumList = announcementListDatumList;
    }

    @Override
    public int getCount() {
        return announcementListDatumList.size();
    }

    @Override
    public Object getItem(int location) {
        return announcementListDatumList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.item_announcement, null);
        Log.e(TAG, convertView.toString());
        textTitle = (TextView) convertView.findViewById(R.id.textAnnouncementTitle);
//        textContent= (TextView) convertView.findViewById(R.id.textAnnouncementContent);
        textPublishDate = (TextView) convertView.findViewById(R.id.textAnnouncementPublishDate);

        // getting consignment list data for the row
        AnnouncementListDatum m = announcementListDatumList.get(position);
        Log.e(TAG, position+"");
        // starting the position from 1 default is 0
        textTitle.setText(m.getTitle());
        mContent = m.getContent();
        textPublishDate.setText(m.getPublish_date());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptContent(position);
            }
        });

        return convertView;
    }

    private void promptContent(int clickPosition) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        String msg  = announcementListDatumList.get(clickPosition).getContent();
        dialogBuilder.setMessage(msg);

        dialogBuilder.setNeutralButton(
                context.getString(R.string.mdtp_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );
        AlertDialog alert = dialogBuilder.create();
        alert.show();
        alert.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.WHITE);
    }

}