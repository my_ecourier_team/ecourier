package com.ps.ecourier.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ps.ecourier.activities.secondLayer.ActivityReportsDetails;
import com.ps.ecourier.R;
import com.ps.ecourier.pojo.ReportsListDatum;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (c) kazi srabon 2016. Contact, kaziiit@gmail.com
 */
public class ReportsListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<ReportsListDatum> reportsListDatumList;

    public ReportsListAdapter(Activity activity, List<ReportsListDatum> reportsListDatumList) {
        this.activity = activity;
        this.reportsListDatumList = reportsListDatumList;
    }

    @Override
    public int getCount() {
        return reportsListDatumList.size();
    }

    @Override
    public Object getItem(int location) {
        return reportsListDatumList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.item_report_list, null);

        TextView consignment_position = (TextView) convertView.findViewById(R.id.textPosition);
        TextView consignment_id = (TextView) convertView.findViewById(R.id.textConsignmentId);
        TextView company = (TextView) convertView.findViewById(R.id.textCompany);
        TextView amount = (TextView) convertView.findViewById(R.id.textProductPrice);
        TextView order_time = (TextView) convertView.findViewById(R.id.textOrderTime);
        TextView parcel_status = (TextView) convertView.findViewById(R.id.textParcelStatus);
        TextView delivered_time = (TextView) convertView.findViewById(R.id.textActualDeliveryTime);

//        TextView sender_group = (TextView) convertView.findViewById(R.id.textSenderGroup);
//        TextView eso_contact = (TextView) convertView.findViewById(R.id.textEsoContact);
//        TextView recipientName = (TextView) convertView.findViewById(R.id.textRecipientName);
//        TextView recipientMobile = (TextView) convertView.findViewById(R.id.textRecipientMobile);
//        TextView address = (TextView) convertView.findViewById(R.id.textRecipientAddress);
//        TextView area = (TextView) convertView.findViewById(R.id.textRecipientArea);
//        TextView requested_delivery_time = (TextView) convertView.findViewById(R.id.textRequestedDeliveryTime);
//        TextView pick_up_agent = (TextView) convertView.findViewById(R.id.textPickupAgent);
//        TextView delivery_agent = (TextView) convertView.findViewById(R.id.textDeliveryAgent);
//        TextView status_code = (TextView) convertView.findViewById(R.id.textStatusCode);
//        TextView shippingPrice = (TextView) convertView.findViewById(R.id.textShippingPrice);
//        TextView codPrice = (TextView) convertView.findViewById(R.id.textCodPrice);
//        TextView paymentMethod = (TextView) convertView.findViewById(R.id.textPaymentMethod);
//        TextView parcelId = (TextView) convertView.findViewById(R.id.textParcelId);
//        TextView comment = (TextView) convertView.findViewById(R.id.textComments);
//        TextView paymentClear = (TextView) convertView.findViewById(R.id.textPaymentClear);
//        TextView reference_id = (TextView) convertView.findViewById(R.id.textReferenceId);
        // getting consignment list data for the row
        ReportsListDatum m = reportsListDatumList.get(position);
// hidden value
//        sender_group.setText(m.getSender_group());
//        eso_contact.setText(m.getEso_contact());
//        recipientName.setText(m.getRecipientName());
//        recipientMobile.setText(m.getRecipientMobile());
//        address.setText(m.getRecipientAddress());
//        area.setText(m.getRecipientArea());
//        requested_delivery_time.setText(m.getRequested_delivery_time());
//        pick_up_agent.setText(m.getPick_up_agent());
//        delivery_agent.setText(m.getDelivery_agent());
//        status_code.setText(m.getStatus_code());
//        shippingPrice.setText(m.getShippingPrice());
//        codPrice.setText(m.getCodPrice());
//        paymentMethod.setText(m.getPaymentMethod());
//        parcelId.setText(m.getParcelId());
////        comment.setText(m.getComment());
//        paymentClear.setText(m.getPaymentClear());
//        reference_id.setText(m.getReference_id());

        // starting the position from 1 default is 0
        consignment_position.setText("" + (position + 1) + ". ");

        // consignment_id
        consignment_id.setText(m.getConsignmentId());
        // company
        company.setText(m.getEso());
        amount.setText("BDT " + m.getProductPrice());
        order_time.setText(m.getOrderTime());
        if(!m.getActual_delivery_time().equals("")){
            delivered_time.setVisibility(View.VISIBLE);
            delivered_time.setText(m.getActual_delivery_time());
        }
        // parcel_status
        parcel_status.setText(m.getParcelStatus());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // convert the list to array list and pass via intent
                ArrayList<ReportsListDatum> ItemArray = ((ArrayList<ReportsListDatum>) reportsListDatumList);

                Intent i = new Intent(activity, ActivityReportsDetails.class);
                i.putExtra("visible_state", "" + 0);
                i.putExtra("consignment_data_position", "" + position);
                i.putExtra("consignment_data_array", ItemArray);
                activity.startActivity(i);

            }
        });

        return convertView;
    }

}